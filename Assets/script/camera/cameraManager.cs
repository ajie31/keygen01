﻿using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;

public class cameraManager : MonoBehaviour
{
    [SerializeField] private CinemachineVirtualCamera _virtualCamera;
    [SerializeField] private Transform player;
    [SerializeField] private gameManager _manager;
    [SerializeField] private listsEnvi[] portals;
    [SerializeField] private textEffect _textTutorial;
    [SerializeField] private GameObject textTutorial;
     private GameObject camera;
    
    private int currentLevel;
    public Transform[] maps;
    public float cameraNormal;
    public float[] cameraTargetZoom;
    
    private WaitForFixedUpdate _wait;
    private float _timeStartedLerping ;
    private float timeSinceStarted ;
    private float percentageComplete;
    private float currentVal;
    private float currentVal1;
    // private float currentVal2;
    private Vector3 currentcamerapos;
    private Camera _camera;
    // Start is called before the first frame update
    void Start()
    {
        currentLevel = _manager.level - 1;
        _virtualCamera.m_LookAt = maps[_manager.level - 1];
        _camera = GetComponent<Camera>();
        camera = _camera.transform.GetChild(0).gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        if (currentLevel != _manager.level - 1)
        {
            _virtualCamera.m_LookAt = maps[_manager.level - 1];
            currentLevel = _manager.level;
        }
    }
    
    public IEnumerator cameraMoveManager( bool zoomOut, float lerpTime = 1f)
    {
        _timeStartedLerping = Time.time;
        timeSinceStarted = Time.time - _timeStartedLerping;
        percentageComplete = timeSinceStarted / lerpTime;
        currentVal = 0;
        currentVal1 = 0;
        yield return 0;
        if (zoomOut)
        {
            _virtualCamera.Follow = null;
            currentcamerapos = _camera.transform.position;
            if (!_manager.pause)
            {
                
                foreach (var portal in portals[currentLevel].PortalCollider2Ds)
                {
                    portal.enabled = false;
                }
            }
          
        }
        else
        {
            camera.SetActive(true);
            currentcamerapos = _camera.transform.position;
           
          

        }

        while (true)
        {
            timeSinceStarted = Time.time - _timeStartedLerping;
            percentageComplete = timeSinceStarted / lerpTime;
            if (zoomOut)
            {
               
                currentVal = Mathf.Lerp(cameraNormal, cameraTargetZoom[currentLevel], percentageComplete);
                _camera.transform.position = Vector3.Lerp(currentcamerapos, new Vector3(0,0,_camera.transform.position.z), percentageComplete);


                //Debug.Log(currentVal);
                // cameraZoomOut = Vector2.Lerp(cameraNormal, cameraTargetZoom, percentageComplete);
            }
            else
            {
                currentVal = Mathf.Lerp(cameraTargetZoom[currentLevel], cameraNormal, percentageComplete);
                _camera.transform.position = Vector3.Lerp(currentcamerapos, new Vector3(player.position.x,player.position.y,_camera.transform.position.z), percentageComplete);

                // cameraZoomOut = Vector2.Lerp(cameraTargetZoom, cameraNormal, percentageComplete);
            }

            _virtualCamera.m_Lens.OrthographicSize = currentVal; 

            if (percentageComplete >= 1)
            {
               
                if (zoomOut) 
                {
                    camera.SetActive(false);
                    if (!_manager.gameOver &&!_manager.pause)
                    {
                       
                        foreach (var portal in portals[currentLevel].PortalCollider2Ds)
                        {
                      
                            portal.enabled = true;
                        }
						
						if(currentLevel == 0)
						{
							textTutorial.SetActive(true);
							_textTutorial.clearTexxt();
							_textTutorial.typeOn = true;
						}
					
                    }
                  
     
                }

                if (!zoomOut)
                {
                    textTutorial.SetActive(false);
                    _virtualCamera.Follow = player;
                    
                }

                

                if (_manager.once && !_manager.playerGoal)
                {
                    _manager.once = false;
                }

               
               
                break;
                
            }

            yield return _wait;
        }
    

       
    }
}
