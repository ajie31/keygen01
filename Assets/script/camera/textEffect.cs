﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class textEffect : MonoBehaviour
{
    [TextArea]
    public string textOutput;

    [Range(0.01f,0.1f)]
    public float intervalSpeed;

    [HideInInspector]
    public bool typeOn; 
    
    private string partialText;
    private float cumulativeDeltaTime;
    
   [SerializeField] private TextMeshProUGUI textType;

    private void Start()
    {
       
        partialText = "";
        cumulativeDeltaTime = 0;
        
    }

    private void Update()
    {
        if (typeOn)
        {
            cumulativeDeltaTime += Time.deltaTime;
            while (cumulativeDeltaTime >= intervalSpeed && partialText.Length < textOutput.Length)
            {
                partialText += textOutput[partialText.Length];
                cumulativeDeltaTime -= intervalSpeed;
               
            }

            textType.text = partialText;
            if (partialText.Length == textOutput.Length)
            {
                typeOn = false;
            }
            
        }
       

    }

    public void clearTexxt()
    {
        partialText = "";
        cumulativeDeltaTime = 0;
        textType.text = "";
    }
}
