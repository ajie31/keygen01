﻿using System;
using UnityEngine;


public class playerMove : MonoBehaviour
{
    public portalMethod[] PortalMethods; 
    [SerializeField] private FloatingJoystick _floating;
    [SerializeField]private gameManager _manager;
    private portalMethod _portal;
    private int currentlevel  = 0;
    private Rigidbody2D rb;
    private Touch _touch;
    private RaycastHit2D _touched;
    private bool once = false;
    public float speed; 
    private Vector3 direction;
    private Vector3 firstTouch;
    private Vector3 deltaPosition;
    private int savetyX = 1;
    private int savetyY = 1;
   // private Vector3 Boundary;
    private float distanceToScreen;
    // Start is called before the first frame update

    //audio Player
   
    private int sfxStats;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        _portal = PortalMethods[_manager.level - 1];
        audioManager._Audio.playSFX(9);
        sfxStats = PlayerPrefs.GetInt("sfxStats");

        if (sfxStats != 0)
        {
            GetComponent<AudioListener>().enabled = false;
        }

    }

    // Update is called once per frame
    void Update()
    {

        if (currentlevel != _manager.level - 1)
        {
            currentlevel = _manager.level - 1;
            _portal = PortalMethods[_manager.level - 1];
        }
        direction = (Vector3.up * _floating.Vertical) + (Vector3.right * _floating.Horizontal);
        if ((_floating.Horizontal != 0 || _floating.Vertical != 0 ) && !once && !_manager.playerGoal)
        {
            once = true;
            audioManager._Audio.Moving = once;
            StartCoroutine(audioManager._Audio.FadeIn(9, .2f));
            
        }
        else if((_floating.Horizontal == 0 || _floating.Vertical == 0 || _manager.playerGoal) && once)
        {
            once = false;
            audioManager._Audio.Moving = once;
           StartCoroutine(audioManager._Audio.FadeOutA(9,.2f));   
        }
        
    }

    private void FixedUpdate()
    {
        if (!_portal.doingTeleport&&!_manager.playerGoal)
        {
          rb.velocity =direction * (speed * Time.fixedDeltaTime);
        }
        else
        {
            rb.velocity = Vector2.zero;
        }
 
       
    }
}
