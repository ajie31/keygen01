﻿
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class playerColliders : MonoBehaviour
{
    public portalMethod[] _MethodsPortals;
    public triggerMethod[] _TriggerMethods;
    public Transform[] StartTransforms;
    private portalMethod _portal;
    private triggerMethod _trigger;
    [SerializeField] private Transform _player;
    [SerializeField] private Transform outline;
    [SerializeField] private LayerMask layer;
    [SerializeField] private gameManager _manager;
    [SerializeField] private Transform deathParticle;

    public RectTransform radialP;
    [SerializeField] private FloatingJoystick joystick;

    private bool gameOver;
    private bool currentStartNode;
    private RaycastHit2D detect;
    private Vector3 getPlayerTrans;
    private Camera mCamera;

    private int currentLevel;

    private bool haveKey;
    //private Image radialProgress;

    private void Start()
    {
        audioManager._Audio.FadeOutA(9, .2f);
        mCamera = Camera.main;
        audioManager._Audio.stopSFX(3);
        _portal = _MethodsPortals[_manager.level - 1];
        _trigger = _TriggerMethods[_manager.level- 1];
        currentStartNode = _trigger.allActivate;
        gameOver = _manager.gameOver;
        // radialProgress = radialP.GetComponent<Image>();
    }

    private void FixedUpdate()
    {
        if (currentLevel!= _manager.level - 1)
        {
            currentLevel = _manager.level - 1;
            _portal = _MethodsPortals[_manager.level - 1];
            _trigger = _TriggerMethods[_manager.level- 1];
        }
        /*  Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, .6f);
         for (int i = 0; i < colliders.Length; i++)
         {
             if (colliders[i].gameObject.CompareTag("portal"))
             {
                 Debug.Log("here");
             }
             
         }*/

       // transform.position = _player.position;
       detect = Physics2D.CircleCast(transform.position, .35f, Vector2.zero,layer);

       if (detect.collider && !_manager.pause)
       {

           if (detect.collider.gameObject.CompareTag("key") && !haveKey)
           {
               audioManager._Audio.playSFX(8);
               haveKey = true;
               outline.position = StartTransforms[currentLevel].position;
               detect.collider.gameObject.SetActive(false);
           }
           if (detect.collider.gameObject.CompareTag("goal")&&!gameOver)
           {
                audioManager._Audio.stopWalk();
                audioManager._Audio.playSFX(10);
               _manager.gameOver = true;
               _manager.playerGoal = true;
               gameOver = true;
           }
           if (detect.collider.gameObject.CompareTag("obstacle") && !gameOver)
           {
               audioManager._Audio.playSFX(7);
               audioManager._Audio.stopWalk();
               
               deathParticle.position = transform.position;
               deathParticle.gameObject.SetActive(true);
               gameOver = true;
               _manager.gameOver = true;
               gameObject.SetActive(false);
              
           }
           if (detect.collider.gameObject.CompareTag("portal") && !_trigger.portalReady && detect.collider.GetComponent<portal>().enterRemain && !_portal.portalDestination)
           {/*
               if (!_portal.insideTeleport)
               {
                   _portal.insideTeleport = true;
                   _portal.doingTeleport = true;
                   joystick.gameObject.SetActive(false);
                   _player.gameObject.SetActive(false);
               }
            */
               radialP.position = mCamera.WorldToScreenPoint(transform.position);
               if (!_trigger.channelPortal)
               {
                   audioManager._Audio.playSFX(3);
                   radialP.gameObject.SetActive(true);
                   _trigger.channelPortal = true;
               }
           }

           if (detect.collider.gameObject.CompareTag("portal") && _trigger.portalReady&& !_portal.portalDestination)
           {
               audioManager._Audio.stopSFX(3);
               audioManager._Audio.playSFX(4);
               radialP.gameObject.SetActive(false);
               _trigger.channelPortal = false;
               detect.collider.GetComponent<portal>().enterRemain = false;
               if (!_portal.insideTeleport)
               {
                   _portal.insideTeleport = true;
                   _portal.doingTeleport = true;
                   joystick.gameObject.SetActive(false);
                   detect.collider.GetComponent<portal>().activeDurability -= 1;
                   _player.gameObject.SetActive(false);
                 
               }
           }
          
           if (detect.collider.gameObject.CompareTag("doorTrigger") && !_trigger.doorOpen)
           {
               radialP.position = mCamera.WorldToScreenPoint(transform.position);
               
               if (!_trigger.insideDoorTrigger)
               {
                   audioManager._Audio.playSFX(3);
                   radialP.gameObject.SetActive(true);
                   _trigger.insideDoorTrigger = true;
               }
           
           }

           if (detect.collider.gameObject.CompareTag("doorTrigger") && _trigger.doorOpen)
           {
               detect.collider.GetComponent<doorTrigger>().doorOpen = true;//transform.GetChild(0).gameObject.SetActive(false);
               audioManager._Audio.stopSFX(3);
               audioManager._Audio.playSFX(6);
               _trigger.insideDoorTrigger = false;
               radialP.gameObject.SetActive(false);
               detect.collider.GetComponent<doorTrigger>().activate = false;
           }
           
           
           if (detect.collider.gameObject.CompareTag("obsTrigger") && !_trigger.obsMove)
           {
               radialP.position = mCamera.WorldToScreenPoint(transform.position);
               
               if (!_trigger.insideObsTrigger)
               {
                   audioManager._Audio.playSFX(3);
                   radialP.gameObject.SetActive(true);
                   _trigger.insideObsTrigger = true;
               }
           
           }

           if (detect.collider.gameObject.CompareTag("obsTrigger") && _trigger.obsMove)
           {
               detect.collider.GetComponent<obsMove>().activateObs = true;//transform.GetChild(0).gameObject.SetActive(false);
               audioManager._Audio.stopSFX(3);
               audioManager._Audio.playSFX(6);
               _trigger.insideObsTrigger = false;
               radialP.gameObject.SetActive(false);
               detect.collider.GetComponent<CircleCollider2D>().enabled = false;
           }
           
           
           if (detect.collider.gameObject.CompareTag("switch") && !_trigger.switchTrigger)
           {
               radialP.position = mCamera.WorldToScreenPoint(transform.position);
               
               if (!_trigger.insideSwitchTrigger)
               {
                   audioManager._Audio.playSFX(3);
                   radialP.gameObject.SetActive(true);
                   _trigger.insideSwitchTrigger = true;
               }
           
           }

           if (detect.collider.gameObject.CompareTag("switch") && _trigger.switchTrigger)
           {
               detect.collider.GetComponent<switchTrigger>().activateSwitch = true;
               audioManager._Audio.stopSFX(3);
               audioManager._Audio.playSFX(6);
               _trigger.insideSwitchTrigger = false;
               radialP.gameObject.SetActive(false);
               detect.collider.GetComponent<CircleCollider2D>().enabled = false;
           }


           
           if (detect.collider.gameObject.CompareTag("activation"))
           {
               radialP.position = mCamera.WorldToScreenPoint(transform.position);

               if (!_trigger.insideActivasion)
               {
                   audioManager._Audio.playSFX(3);
                   radialP.gameObject.SetActive(true);
                   _trigger.insideActivasion = true;
               }
           }

           if (currentStartNode != _trigger.allActivate)
           {
               audioManager._Audio.stopSFX(3);
                radialP.gameObject.SetActive(false);
                currentStartNode = _trigger.allActivate;
                detect.collider.GetComponent<startingPoint>().activeStart = currentStartNode;
                if (!currentStartNode && haveKey)
                {
                    _manager.guidingLightTggl = true;
                    StartCoroutine(_manager.guideLight());
                }
                if (currentStartNode && haveKey)
                {
                    _manager.guidingLightTggl = false;
                    
                }
           }
       }

       if ((!detect.collider && (_trigger.insideActivasion || _trigger.insideDoorTrigger || _portal.insideTeleport ||
                                 _trigger.channelPortal || _trigger.insideObsTrigger|| _trigger.insideSwitchTrigger))||_manager.pause)
       {
           audioManager._Audio.stopSFX(3);
           if (_trigger.insideActivasion )
           {
               radialP.gameObject.SetActive(false);
               _trigger.insideActivasion = false;
           }

           if (_trigger.insideDoorTrigger)
           {
               radialP.gameObject.SetActive(false);
               _trigger.insideDoorTrigger = false;
           }
           
           if ( _trigger.insideObsTrigger)
           {
               radialP.gameObject.SetActive(false);
               _trigger.insideObsTrigger = false;
               
           }
            
           if ( _trigger.insideSwitchTrigger)
           {
               radialP.gameObject.SetActive(false);
               _trigger.insideSwitchTrigger = false;
               
           }

           if (_trigger.channelPortal)
           {
               radialP.gameObject.SetActive(false);
               _trigger.channelPortal = false;
           }

           if (!_manager.pause)
           {
               if ( _portal.insideTeleport)
               {
           
                   _portal.portalDestination = false;
                   _portal.insideTeleport = false;
               }
           }
          
       }
       

     
    }
    
    
}
