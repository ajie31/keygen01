﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;


public class recapAct : MonoBehaviour
{
    public TextMeshProUGUI bigText;
    public TextMeshProUGUI timerText;
    public TextMeshProUGUI secret;
    public GameObject recordText;
    public CanvasGroup cg;
    public Animator starAnim;
    public textEffect _texting;
    private int timer;
    private int stars;
    private string bigTextMessage;
    private WaitForFixedUpdate _fixedUpdate;
    private WaitForSeconds _seconds;
    private float timingFade = 0;
    private string goal = "SPySek";
    // Start is called before the first frame update
    void Start()
    {
        recapScore._recap.scoreCheck(recordText);
        timer = recapScore._recap.getTime();
        stars = recapScore._recap.getStar();
        StartCoroutine(audioManager._Audio.FadeInBGM(3, 2, .6f));
        bigTextMessage = recapScore._recap.getText();
        _texting.textOutput = bigTextMessage;
        _fixedUpdate = new WaitForFixedUpdate();
        _seconds = new WaitForSeconds(2f);
        checkStar();
        secret.text = goal[recapScore._recap.getIndex()].ToString();
        StartCoroutine(canvasFade());
    }

    private IEnumerator canvasFade()
    {
       // StartCoroutine(audioManager._Audio.recapSound());
        while (true)
        {
            timingFade += Time.deltaTime/2.5f;
            cg.alpha = Mathf.Lerp(0f, 1f, timingFade);
            if (timingFade >= 1)
            {
                timingFade = 0;
                StartCoroutine(countTime());
                break;
            }
            yield return _fixedUpdate;
        }        
    
    }

    private void checkStar()
    {
        if (stars == 3)
        {
            starAnim.SetBool("threeStar", true);
            starAnim.SetBool("twoStar", true);
        }
        else if (stars == 2)
        {
            starAnim.SetBool("twoStar", true);
        }
       

    }

    private IEnumerator countTime()
    {
        timerTic._timer.resetTimer();
        int i = 0;
        audioManager._Audio.playSFX(19);
        while (true)
        {
            
            timerText.text = timerTic._timer.timeConvert(i);
            i++;
            if (i == timer)
            {
                audioManager._Audio.stopSFX(19);
                break;
            }
            yield return null;

        }
        starAnim.SetFloat("play", 1);
        yield return _seconds;
        //textTutorial.SetActive(true);
        _texting.clearTexxt();
        _texting.typeOn = true;
    }
    public void mainMenuBrr()
    {

        // TIMER PlayerPrefs.SetFloat("time", timer);

        timerTic._timer.stopCounter();
       
        audioManager._Audio.playSFX(12);
        SceneManager.LoadScene(1);
        //SceneManager.LoadScene(0);
    }
    public void restartBtt()
    {

        audioManager._Audio.playSFX(12);
        timerTic._timer.stopCounter();
        SceneManager.LoadScene(2);
        //  SceneManager.LoadScene(1);
        if (Application.platform == RuntimePlatform.Android)
        {
            adsManager._Ads.showInterstitial();
        }
    }
    // Update is called once per frame

}
