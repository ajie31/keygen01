﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class recapScore : MonoBehaviour
{
    public static recapScore _recap;
    private int stars;
    private int timeVal;
    private string starsId = "starsLv";
    private string timeId = "timeLv";
    private int index;
    private string textStar;

    private void Awake()
    {
        if (_recap == null)
        {
            DontDestroyOnLoad(this);
            _recap = this;
            
        }
        else
        {
            if (_recap != this)
            {
                Destroy(this);
            }
        }
    }


    public void countStar (int target,int target1)
    {
        int oldStar = PlayerPrefs.GetInt(starsId + index);
        if (timeVal <= target)
        {
            stars = 3;
            textStar = "Perfect!";
        }
        else if (timeVal >= target && timeVal <= target1)
        {
            stars = 2;
            textStar = "Almost There";
        }
        else
        {
            stars = 1;
            textStar = "2 Star Remain";
        }

        if (oldStar < stars || oldStar == 0)
        {
            
            PlayerPrefs.SetInt(starsId + index.ToString(), stars);
           
        }
        

    }

    public void scoreCheck( GameObject text)
    {
        int _time = PlayerPrefs.GetInt(timeId + index.ToString());

        if (_time == 0)
        {
            PlayerPrefs.SetInt(timeId + index.ToString(), timeVal);
            text.SetActive(true);
        }
        else if(timeVal < _time && _time != 0)
        {
            PlayerPrefs.SetInt(timeId + index.ToString(), timeVal);
            text.SetActive(true);
        }
        

    }

    public void setTime(int time)
    {
        timeVal = time;

    }
    public int getTime()
    {
        return timeVal;
    }

    public string getText()
    {

        return textStar;
    }
    
    public int getStar()
    {
        return stars;
    }
    public void setIndex(int id)
    {
        index = id;
    }

    public int getIndex()
    {
        return index;
    }

}
