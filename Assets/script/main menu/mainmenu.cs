﻿
using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using System.Collections;
using UnityEngine.UI;
using TMPro;

public class mainmenu : MonoBehaviour
{
    private float delayStart;
    public GameObject player;
    public playercollidermainmenu _playerCol;
    public bool reswnNow;
    // play button
    public bool insidePlay;
    public bool activatePlay;
	//sfx controller
	public bool insideMutesfX;
	public bool activateMutesfx;
	//music controller
	public bool insideMuteMusic;
	public bool activateMuteMusic;
    //levelMenu
    private int levels;
    public bool insideLevel;
    public bool activateLevel;

    //guideBook
    public bool insideGuidebook;
	public bool activateGuideBook;
	public GameObject guideBook;
	public GameObject nextBtt;
	public GameObject prevBtt;
	public TextMeshProUGUI Title;
	
	private int pageGuideBook = 0;
	public GameObject[] guideBookPage;
	//end guide book
    //general
    public Image radialProgress;
    public Image overlay;
    public level[] _levels;
    public GameObject _adminTools;
    public Animator title;
    public Animator mainCamera;
    public Animator letter;

    private float radialAmount;
    private bool once =false;
    private WaitForFixedUpdate _waitForFixedUpdate;
    private WaitForSeconds _forSeconds;
    //private WaitForSeconds _forSeconds;
    private Color startColor;
    [SerializeField] private Color target;
    private int _resetScene;
    private float playAnimation = 1;
   
    private Vector3 playerPos;
    //end general

    //UI MANAGER
    private int guideOpened;
    private int musicStats;
    private int sfxStats;

    public Animator guideBookAnimator;
    public Animator musicAnimator;
    public Animator sfxAnimator;
	    
    //exit apps
    private int exitCount = 1;
    public GameObject exitText;
    private bool onceExit =false;

    //levelStage
    private int unlockLevel;

   // public mainmenu _main;
    private int input;
    // Start is called before the first frame update
    private void Start()
    {
        StartCoroutine(audioManager._Audio.FadeOutBGM(0, 3));
        StartCoroutine(audioManager._Audio.FadeOutBGM(3, 3));
        levels = PlayerPrefs.GetInt("levelUnlock");
       // demo PlayerPrefs.SetInt("level", 2);
        _waitForFixedUpdate = new WaitForFixedUpdate();
        _forSeconds = new WaitForSeconds(2);
        StartCoroutine(fading(true));
        playerPos = new Vector3(0,1.5f,0);

        musicStats = PlayerPrefs.GetInt("musicStats");
        sfxStats = PlayerPrefs.GetInt("sfxStats");
        guideOpened = PlayerPrefs.GetInt("guideBookStats");
        StartCoroutine(audioManager._Audio.FadeInBGM(6,50,.6f));
        StartCoroutine(audioManager._Audio.FadeInBGM(7,50, .6f));
        if (guideOpened == 0)
        {
	        guideBookAnimator.Play("guideBook",-1,0);
	        guideBookAnimator.SetFloat("guideBookPlay",1f);
        }
        StartCoroutine(unlockLevels());

    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) )
        {
            if (exitCount > 0 && !onceExit)
            {
                exitCount-= 1;
                onceExit = true;
                StartCoroutine(exitDialogue());
            }

            else if (exitCount == 0)
            {
                Application.Quit();
            }
        }

        if((insidePlay||insideGuidebook||insideMuteMusic||insideMutesfX||insideLevel) && radialAmount < 1)
        {
            if (!once)
            {
                radialProgress.gameObject.SetActive(true);
                once = true;
            }

            radialAmount += Time.deltaTime;
            radialProgress.fillAmount = radialAmount;
        }

        if ((!insidePlay && !insideGuidebook && !insideMuteMusic && !insideMutesfX && !insideLevel) && once)
        {
            if (radialAmount >= 1 && (activatePlay || activateGuideBook || activateMuteMusic || activateMutesfx || activateLevel))
            {
                activatePlay = false;
				activateGuideBook = false;
				activateMuteMusic = false;
				activateMutesfx = false;
                activateLevel = false;
                radialAmount = 0;
            }

            if (radialAmount < 1)
            {
                radialAmount = 0;
                radialProgress.fillAmount = radialAmount;
                radialProgress.gameObject.SetActive(false);
            }

            once = false;
        }

        if (insidePlay && radialAmount >= 1 && !activatePlay)
        {
            activatePlay = true;
        } 
        if (insideGuidebook && radialAmount >= 1 && !activateGuideBook)
        {
            activateGuideBook = true;
        }
	    if (insideMuteMusic && radialAmount >= 1 && !activateMuteMusic)
        {
            activateMuteMusic = true;
        }
	    if (insideMutesfX && radialAmount >= 1 && !activateMutesfx)
        {
            activateMutesfx = true;
        }
        if (insideLevel && radialAmount >= 1 && !activateLevel)
        {
            activateLevel = true;
        }

        if (reswnNow)
        {
            StartCoroutine(respawn());
            reswnNow = false;
        }
        
        
    }
  

    public IEnumerator fading(bool fadeIn)
    {
		
        delayStart = 0;
        if (fadeIn)
        {
            startColor = overlay.color;
            yield return _forSeconds;
           
        }
        else
        {
            overlay.gameObject.SetActive(true);
            target = startColor;
            startColor = overlay.color;
            //Debug.Log(startColor);
        }

      
        while (true)
        {
            delayStart += Time.deltaTime /1;
            overlay.color = Color.Lerp(startColor, target, delayStart);

            if (overlay.color.a <= 0 && fadeIn)
            {
                title.SetFloat("start",playAnimation);
                letter.SetFloat("start",playAnimation);
                mainCamera.SetFloat("start",playAnimation);		
                adsManager._Ads.RequestBanner();
               // adsManager._Ads.showBanner0();
                overlay.gameObject.SetActive(false);
                break;
            }
            if (overlay.color.a >= 1 && !fadeIn)
            {
                // demo and real
                yield return null;
                if(PlayerPrefs.GetInt("level") != 7)
                {
                    SceneManager.LoadScene(2);
                    //SceneManager.LoadScene(1);
                }
                else
                {
                    SceneManager.LoadScene(3);
                    //SceneManager.LoadScene(2);
                }
              // _adminTools.SetActive(true);
                break;
            }

            yield return _waitForFixedUpdate;
        }

    }

    public IEnumerator respawn()
    {
        overlay.gameObject.SetActive(true);
        yield return _forSeconds;
        player.transform.position = playerPos;
        player.SetActive(true);
		if(!activateGuideBook){
			overlay.gameObject.SetActive(false);
		}
        
        _playerCol.respawn = false;

    }
    
    public void adminTools(Button btn){
        input = Convert.ToInt16(btn.name);
        PlayerPrefs.SetInt("level", input);
        SceneManager.LoadScene(2);
        // SceneManager.LoadScene(1);
    }
    public void finaleBtt(){

        SceneManager.LoadScene(3);
        // SceneManager.LoadScene(2);
    }
	
	public void activateGuide(){
		
		Title.text = "Generals";
        adsManager._Ads.hideDestroyBanner(false);
		guideBook.SetActive(true);
		guideBookPage[0].SetActive(true);
		overlay.gameObject.SetActive(true);
		overlay.color = startColor;
	}
	
	public void closeGuide(){

        adsManager._Ads.showBanner0();
		guideBookPage[1].SetActive(false);
		guideBookPage[2].SetActive(false);
		guideBook.SetActive(false);
		overlay.gameObject.SetActive(false);
		overlay.color = target;
		guideDisableAnim();
		
	}
	
	public void nextPage(){
		if(pageGuideBook >= 0){
			if(pageGuideBook == 0){
			
				prevBtt.SetActive(true);
		
			}
			guideBookPage[pageGuideBook].SetActive(false);
			pageGuideBook ++;
			guideBookPage[pageGuideBook].SetActive(true);
			
		
		}
		if(pageGuideBook == 1){
			Title.text = "Portals";

		}
		if(pageGuideBook == 2){
			Title.text = "Indicator Logo";
			nextBtt.SetActive(false);
		
		}
		

	}
	public void prevPage(){
		
		if(pageGuideBook >= 0){
			if(pageGuideBook == 2){
			
				nextBtt.SetActive(true);
		
			}
			guideBookPage[pageGuideBook].SetActive(false);
			pageGuideBook --;
			guideBookPage[pageGuideBook].SetActive(true);
		}
		
		if(pageGuideBook == 1){
			Title.text = "Portals";
		}
		
		if(pageGuideBook == 0){
			Title.text = "Generals";
			prevBtt.SetActive(false);
		}
	}

	public void musicUI()
	{
		if (musicStats == 0)
		{
			musicAnimator.Play("musicUI",-1,0);
			musicAnimator.SetFloat("musicPlay",1f);
			musicStats = 1;
			PlayerPrefs.SetFloat("musicStats",musicStats);
		}
		else
		{
			musicAnimator.Play("musicUI",-1,1);
			musicAnimator.SetFloat("musicPlay",-1f);
			musicStats = 0;
			PlayerPrefs.SetFloat("musicStats",musicStats);
		}
	
	}
	public void sfxUI()
	{
		if (sfxStats == 0)
		{
			sfxAnimator.Play("sfxUI",-1,0);
			sfxAnimator.SetFloat("sfxPlay",1f);
			sfxStats = 1;
			PlayerPrefs.SetFloat("sfxStats",musicStats);
		}
		else
		{
			sfxAnimator.Play("sfxUI",-1,1);
			sfxAnimator.SetFloat("sfxPlay",-1f);
			sfxStats = 0;
			PlayerPrefs.SetFloat("sfxStats",musicStats);
		}
	
	}

	private void guideDisableAnim()
	{
		
		guideBookAnimator.Play("guideBook",-1,0);
		guideBookAnimator.SetFloat("guideBookPlay",0f);
		guideOpened = 1;
		PlayerPrefs.SetInt("guideBookStats",guideOpened);
	}

	private IEnumerator exitDialogue()
    {
        exitText.SetActive(true);
        yield return _forSeconds;
        exitText.SetActive(false);
        onceExit = false;
        exitCount = 1;
    }

    public IEnumerator unlockLevels()
    {
        yield return _forSeconds;
        for (int i = 0; i <= levels ; i++)
        {
            _levels[i].activateLevel = true;
            
            yield return null;
        }
    }
	
}
