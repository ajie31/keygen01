﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playercollidermainmenu : MonoBehaviour
{
    private RaycastHit2D detect;
    
    public RectTransform radialP;
    public mainmenu _Mainmenu;
    public cameraMove _cMove;
    private playerMoveMainmenu _pMove;
    public Transform deathParticle;
    public bool respawn;

    private int bgmStat;
    private int sfxStat;
    private Camera mCamera;
	private bool hasActivate;

    private AudioListener _listener;
    // Start is called before the first frame update
    void Start()
    {
        mCamera = Camera.main;
        _pMove = GetComponent<playerMoveMainmenu>();
        bgmStat = PlayerPrefs.GetInt("musicStats");
        _listener = GetComponent<AudioListener>();
        sfxStat = PlayerPrefs.GetInt("sfxStats");
    }

    // Update is called once per frame
    void Update()
    {
        detect = Physics2D.CircleCast(transform.position, .2f, Vector2.zero);

        if (detect.collider)
        {
            if (detect.collider.gameObject.CompareTag("obstacle") && ! respawn)
            {
                audioManager._Audio.playSFX(7);
                _Mainmenu.reswnNow = true;
                deathParticle.position = transform.position;
                deathParticle.gameObject.SetActive(true);
                respawn = true;
                gameObject.SetActive(false);
              
            }
            if (detect.collider.gameObject.CompareTag("play") && !_Mainmenu.activatePlay)
            {
                
                radialP.position = mCamera.WorldToScreenPoint(transform.position);
               
                if (!_Mainmenu.insidePlay)
                {
                    audioManager._Audio.playSFX(3);
                    radialP.gameObject.SetActive(true);
                    
                    _Mainmenu.insidePlay = true;
                }
           
            }

            if (detect.collider.gameObject.CompareTag("play") && _Mainmenu.activatePlay)
            {
               //transform.GetChild(0).gameObject.SetActive(false);
              
               _Mainmenu.insidePlay = false;
               radialP.gameObject.SetActive(false);
               audioManager._Audio.stopSFX(3);
                // detect.collider.GetComponent<CircleCollider2D>().enabled = false;
                detect.collider.GetComponent<playButton>().playNow = true;
              //  StartCoroutine(_pMove.moveTo());
              //   StartCoroutine(_cMove.cameraMoveBott());
                if (!variableUniversal._var.mainMenuStatus)
                {
                    variableUniversal._var.mainMenuStatus = true;
                }

                audioManager._Audio.playSFX(6);


            }
			
			if (detect.collider.gameObject.CompareTag("music") && !_Mainmenu.activateMuteMusic && !hasActivate)
            {
                
                radialP.position = mCamera.WorldToScreenPoint(transform.position);
               
                if (!_Mainmenu.insideMuteMusic)
                {
                    audioManager._Audio.playSFX(3);
                    radialP.gameObject.SetActive(true);
                    
                    _Mainmenu.insideMuteMusic = true;
                }
           
            }

            if (detect.collider.gameObject.CompareTag("music") && _Mainmenu.activateMuteMusic && !hasActivate)
            {
               //transform.GetChild(0).gameObject.SetActive(false);
				hasActivate = true;
                _Mainmenu.insideMuteMusic = false;
                radialP.gameObject.SetActive(false);
                audioManager._Audio.stopSFX(3);
                if (bgmStat == 0)
                {
                    PlayerPrefs.SetInt("musicStats", 1);
                    bgmStat = 1;
                    audioManager._Audio.bgmEnable = false;
                    audioManager._Audio.stopBGM(6);
                    audioManager._Audio.stopBGM(7);
                }
                else
                {
                    PlayerPrefs.SetInt("musicStats", 0);
                    bgmStat = 0;
                   
                    audioManager._Audio.bgmEnable = true;
                    StartCoroutine(audioManager._Audio.FadeInBGM(6, 1, .6f));
                    StartCoroutine(audioManager._Audio.FadeInBGM(7, 1, .6f));
                }
                audioManager._Audio.musicStats = bgmStat;
                //detect.collider.GetComponent<CircleCollider2D>().enabled = false;
                //StartCoroutine(_Mainmenu.fading(false));
              // audioManager._Audio.playSFX(0);
                

            }
			
			if (detect.collider.gameObject.CompareTag("sfx") && !_Mainmenu.activateMutesfx && !hasActivate)
            {
                
                radialP.position = mCamera.WorldToScreenPoint(transform.position);
               
                if (!_Mainmenu.insideMutesfX)
                {
                    audioManager._Audio.playSFX(3);
                    radialP.gameObject.SetActive(true);
                    
                    _Mainmenu.insideMutesfX = true;
                }
           
            }

            if (detect.collider.gameObject.CompareTag("sfx") && _Mainmenu.activateMutesfx && !hasActivate)
            {
               //transform.GetChild(0).gameObject.SetActive(false);
				hasActivate = true;
                _Mainmenu.insideMutesfX = false;
                radialP.gameObject.SetActive(false);
                audioManager._Audio.stopSFX(3);

                if (sfxStat == 0)
                {
                    PlayerPrefs.SetInt("sfxStats", 1);
                    sfxStat = 1;
                    _listener.enabled = false;
                   
                }
                else
                {
                    PlayerPrefs.SetInt("sfxStats", 0);
                    sfxStat = 0;
                    _listener.enabled = true;
                   
                }

                audioManager._Audio.sfxStats = sfxStat;
                //detect.collider.GetComponent<CircleCollider2D>().enabled = false;
                // StartCoroutine(_Mainmenu.fading(false));
                // audioManager._Audio.playSFX(0);


            }
			if (detect.collider.gameObject.CompareTag("guideBook") && !_Mainmenu.activateGuideBook && !hasActivate)
            {
                
                radialP.position = mCamera.WorldToScreenPoint(transform.position);
               
                if (!_Mainmenu.insideGuidebook)
                {
                    audioManager._Audio.playSFX(3);
                    radialP.gameObject.SetActive(true);
                    
                    _Mainmenu.insideGuidebook = true;
                }
           
            }

            if (detect.collider.gameObject.CompareTag("guideBook") && _Mainmenu.activateGuideBook && !hasActivate)
            {
               //transform.GetChild(0).gameObject.SetActive(false);
				hasActivate = true;
				
				 
				 _Mainmenu.activateGuide();
                _Mainmenu.insideGuidebook = false;
                radialP.gameObject.SetActive(false);
                audioManager._Audio.stopSFX(3);
            

            }

            if (detect.collider.gameObject.CompareTag("level") && !_Mainmenu.activateLevel && !hasActivate)
            {

                radialP.position = mCamera.WorldToScreenPoint(transform.position);

                if (!_Mainmenu.insideLevel)
                {
                    audioManager._Audio.playSFX(3);
                    radialP.gameObject.SetActive(true);

                    _Mainmenu.insideLevel = true;
                }

            }

            if (detect.collider.gameObject.CompareTag("level") && _Mainmenu.activateLevel && !hasActivate)
            {
                //transform.GetChild(0).gameObject.SetActive(false);
                detect.collider.GetComponent<level>().levelStart = true;
                hasActivate = true;
                _Mainmenu.insideLevel = false;
                radialP.gameObject.SetActive(false);
                audioManager._Audio.playSFX(0);
                audioManager._Audio.stopSFX(3);
                StartCoroutine(_Mainmenu.fading(false));


            }
        }

     

        if (!detect.collider && (_Mainmenu.insidePlay || _Mainmenu.insideMutesfX || _Mainmenu.insideGuidebook ||  _Mainmenu.insideMuteMusic || _Mainmenu.insideLevel || hasActivate ) )
        {
            audioManager._Audio.stopSFX(3);
            radialP.gameObject.SetActive(false);
            _Mainmenu.insidePlay = false;
			_Mainmenu.insideMutesfX = false;
			_Mainmenu.insideGuidebook = false;
			_Mainmenu.insideMuteMusic = false;
            _Mainmenu.insideLevel = false;
			hasActivate = false;
            //Debug.Log("here");
        }
    }
}
