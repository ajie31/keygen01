﻿using System;
using UnityEngine.SceneManagement;
using UnityEngine;
using TMPro;

public class level : MonoBehaviour
{
    public bool activateLevel;
    private bool currentStat;
    private Animator _level7Anim;
    public GameObject[] stars;
    public bool levelStart;
    public TextMeshPro _levelText;
    public bool PlayPass;
    private SpriteRenderer _logo;
    private CircleCollider2D _collider2D;
    private SpriteRenderer _renderer;
    private int input,star;
    private string code = "SPySek";
    // Start is called before the first frame update
    void Start()
    {
        input = Convert.ToInt16(gameObject.name);
        _collider2D = GetComponent<CircleCollider2D>();
        _renderer = GetComponent<SpriteRenderer>();
        _collider2D.enabled = false;
        _renderer.color = variableUniversal._var.getColorLevel(activateLevel);
        if (PlayPass)
        {
            _level7Anim = GetComponent<Animator>();
            _logo = transform.GetChild(0).GetComponent<SpriteRenderer>();
            _logo.color = variableUniversal._var.getColorLevel(activateLevel);
        }
        else
        {
            _levelText.color = variableUniversal._var.getColorLevel(activateLevel);
            SetStars();
        }
       
    }

    // Update is called once per frame
    void Update()
    {
        if ((currentStat != activateLevel))
        {
            if (!activateLevel)
            {
              
                currentStat = activateLevel;
        
                _collider2D.enabled = false;
                _renderer.color = variableUniversal._var.getColorLevel(activateLevel);
                if (PlayPass)
                {
                    _logo.color = variableUniversal._var.getColorLevel(activateLevel);
                }
                else
                {
                    _levelText.color = variableUniversal._var.getColorLevel(activateLevel);
                }
               

            }
            else
            {

                currentStat = activateLevel;
               
                _collider2D.enabled = true;
                _renderer.color = variableUniversal._var.getColorLevel(activateLevel);
                if (PlayPass)
                {
                    _level7Anim.SetFloat("play", 1);
                    _logo.color = variableUniversal._var.getColorLevel(activateLevel);
                }
                else
                {
                    _levelText.color = variableUniversal._var.getColorLevel(activateLevel);
                }

            }
        }

        if (levelStart)
        {
           
            PlayerPrefs.SetInt("level", input);
           
            levelStart = false;
            _collider2D.enabled = false;
        }
    }

    private void SetStars()
    {
        
        star = PlayerPrefs.GetInt("starsLv" + (input - 1));
        for (int i = 0; i < star; i++)
        {
            stars[i].SetActive(true);
        }

        if (star == 3)
        {
            _levelText.text = code[input - 1].ToString();
        }

    }
}
