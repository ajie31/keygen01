﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playButton : MonoBehaviour
{
    public bool playNow;
    public bool openDoor;
    private bool currentStat;
    private bool currentDoor;

    private SpriteRenderer _renderer;
    private SpriteRenderer _rendererWire;
    private CircleCollider2D _collider2D;
    private GameObject door;

    private void Start()
    {
        door = transform.GetChild(1).gameObject;
        _renderer = GetComponent<SpriteRenderer>();
        _collider2D = GetComponent<CircleCollider2D>();
        _rendererWire = transform.GetChild(2).GetComponent<SpriteRenderer>();
        openDoor = variableUniversal._var.mainMenuStatus;
        if (openDoor)
        {
            door.SetActive(false);

            _collider2D.enabled = false;
            _renderer.color = variableUniversal._var.getColorLevel(playNow);

            _rendererWire.color = variableUniversal._var.getColorLevel(playNow);
        }
      
    }

    private void Update()
    {
        if ((currentStat != playNow))
        {
            if (playNow)
            {
               
                currentStat = playNow;

               door.SetActive(false);

                _collider2D.enabled = false;
                _renderer.color = variableUniversal._var.getColorLevel(false);
             
                _rendererWire.color = variableUniversal._var.getColorLevel(false);
            }
        
        }

       /* if (currentDoor != openDoor)
        {
            currentDoor = openDoor;
            door.SetActive(false);
            _rendererWire.color = environmentStatic.current.getColorWD(false);
        }*/
    }
}
