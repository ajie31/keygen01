﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerMoveMainmenu : MonoBehaviour
{
    [SerializeField] private FloatingJoystick _floating;
    public float speed;
    private Vector3 direction;
    private bool once;
    private Rigidbody2D rb;

    public Vector3 targetMovePlayer;
    public Vector3 initialPlayer;
    private float step;

    private int sfxStats;

    // Start is called before the first frame update
    void Start()
    {
        if (variableUniversal._var.mainMenuStatus)
        {
            transform.position = targetMovePlayer;
        }
        else
        {
            transform.position = initialPlayer;
        }

        sfxStats = PlayerPrefs.GetInt("sfxStats");

        if (sfxStats != 0)
        {
            GetComponent<AudioListener>().enabled = false;
        }

        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        direction = (Vector3.up * _floating.Vertical) + (Vector3.right * _floating.Horizontal);
        if (rb.velocity != Vector2.zero && !once)
        {
            StartCoroutine(audioManager._Audio.FadeIn(9, .2f));
            once = true;
        }
        else if(rb.velocity == Vector2.zero && once)
        {
            StartCoroutine(audioManager._Audio.FadeOutA(9,.2f));
           
            once = false;
        }
    }

    private void FixedUpdate()
    {
        
        rb.velocity =direction * (speed * Time.fixedDeltaTime);
        
    }

   
}
