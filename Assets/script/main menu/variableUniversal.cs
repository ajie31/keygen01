﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class variableUniversal : MonoBehaviour
{
    public static variableUniversal _var;
   
    public bool mainMenuStatus;
    public Color triggerActive;
    public Color triggerDeactive;
    

    private void Awake()
    {
        if (_var == null)
        {
            DontDestroyOnLoad(this);
            _var = this;
        }
        else
        {
            if (_var != this)
            {
                Destroy(this);
            }
        }
       
  
    }


    public void setLevelUnlock(int setLevel)
    {
        PlayerPrefs.SetInt("levelUnlock", setLevel);

    }

    public void setStatusMainmenu(bool stats)
    {
        mainMenuStatus = stats;
    }
    public bool getStatusMainmenu()
    {
        return mainMenuStatus;
    }

    public Color getColorLevel(bool active)
    {
        if (active)
        {
            return triggerActive;
        }
        else
        {
            return triggerDeactive;
        }
        
    }

}
