﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class timerTic : MonoBehaviour
{
    public static timerTic _timer;
    private int timer = 0;
    private bool itsStarting;
    private Slider progressSlider;
    private WaitForSeconds _waitScenods;
    private WaitForFixedUpdate _fixedUpdate;
    private float progressVal;
    
    private int target;
    private int target1;
    private Animator[] _animatorStar;
    //timer
    //private static float timer;
    private int second, minute, hour;
    private string anim = "play";// anim2Name = "starFadeOut", anim3Name = "star2FadeOut";
    private TextMeshProUGUI timerText;
    // Start is called before the first frame update

    private void Awake()
    {
        if (_timer == null)
        {
            DontDestroyOnLoad(this);
            _timer = this;
        }
        else
        {
            if (_timer != this)
            {
                Destroy(this);
            }
        }
    }

    private void Start()
    {
        // target = 120;
        _waitScenods = new WaitForSeconds(1);
        _fixedUpdate = new WaitForFixedUpdate();

    }

    // Update is called once per frame

    //REFERENCES SLIDER

    public void setSliderandText(Slider _slider, TextMeshProUGUI _timerText)
    {
        progressSlider = _slider;
        timerText = _timerText;
    }
    //reset timer to 0
    public void resetTimer()
    {
        timer = 0;
        second = 0;
        minute = 0;
        hour = 0;
    }

    //set best targetTime
    public void setTargetTime(int _target)
    {
        target = _target;
    }

    public void setTargetTime1(int _target)
    {
        target1 = _target;
    }
    // timer manager
    public void stopCounter()
    {
        itsStarting = false;
    }

    public void startCounter()
    {
        itsStarting = true;
        StartCoroutine(startProgress());

    }
    public void setAnimator(Animator[] _animator)
    {
        _animatorStar = _animator;
    }

    //end timer manager
    public void setTimerOnMemory()
    {
        PlayerPrefs.SetFloat("time", timer);

    }
    public string timeConvert(int i)
    {
        second = i;

        if (second >= 60f)
        {

            minute = (second / 60);
            second = second % 60;

            if (minute >= 60)
            {

                hour = (minute / 60);
                minute = minute % 60;

            }

        }
        return hour.ToString("00") + ":" + minute.ToString("00") + ":" + second.ToString("00");

        
        //timerText.text = hour.ToString("00") + ":" + minute.ToString("00") + ":" + second.ToString("00");

    }

    public int getTime()
    {

        return timer;
    }

    public void timerShow()
    {
        second = timer;
        if (second >= 60f)
        {

            minute = (second / 60);
            second = second % 60;

            if (minute >= 60)
            {

                hour = (minute / 60);
                minute = minute % 60;

            }

        }
        
        timerText.text = hour.ToString("00") + ":" + minute.ToString("00") + ":" + second.ToString("00");

    }
    // Update is called once per frame
    private IEnumerator startProgress()
    {
        
        float valTimer = timer;
        bool animAct = false;
        bool animAct1 = false;
        timer = 0;
        progressVal = 1;
        // float slideVal = target / valB;
        yield return _waitScenods;
        while (true)
        {
            if (progressVal <= 0.1f || !itsStarting)
            {
                
                break;
            }
          
            if (timer >= target)
            {
                valTimer = timer;
                progressVal = target / valTimer;
                progressSlider.value = progressVal;
            }

            if(timer >= target && !animAct)
            {
               
                _animatorStar[0].SetFloat(anim, 1);
                animAct = true;
            }
            else if(timer >= target1 && !animAct1 )
            {
                _animatorStar[1].SetFloat(anim, 1);
               
                animAct1 = true;
            }


            timer += 1;
            timerText.text = timeConvert(timer);
            // timerShow();
            //timing += Time.deltaTime / 2;
            yield return _waitScenods;
        }

    }
  

}
