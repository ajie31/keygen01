﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraMove : MonoBehaviour
{
    [SerializeField] private Transform player;
    [SerializeField] private Vector3 targetCamera;
    [SerializeField] private Vector3 initialCamera;
    [SerializeField] private float speed;
    public bool cameraActive;
    private Camera mainCamera;
    private bool onceUpdate;

    private Vector3 startOffset;
    private Vector3 midCamera;
    private Vector3 cameraM;

    private float step;
    
    // Start is called before the first frame update
    void Start()
    {
      
        
        mainCamera = GetComponent<Camera>();
        midCamera = mainCamera.ScreenToWorldPoint(new Vector3(Screen.width / 2, Screen.height / 2, mainCamera.nearClipPlane));
        startOffset = transform.position - new Vector3(0, player.position.y, 0);
        if (variableUniversal._var.mainMenuStatus)
        {
            transform.position = targetCamera;
        }
        else
        {
            transform.position = initialCamera;
        }


        // Debug.Log(midCamera);
    }

    // Update is called once per frame
    void FixedUpdate()
    {


        if (player.position.y <= midCamera.y)
        {

            if (!onceUpdate)
            {
                startOffset = transform.position - new Vector3(0, player.position.y, 0);
                onceUpdate = true;
            }
            cameraM = new Vector3(0, player.position.y,0) + startOffset;
       
            transform.position = cameraM;

        }
        else if(player.position.y <= midCamera.y && onceUpdate)
        {
            onceUpdate = false;
        }
      
    }

   
}
