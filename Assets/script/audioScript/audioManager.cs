﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.Audio;

public class audioManager : MonoBehaviour
{
    public static audioManager _Audio;
    public audio [] bgmAudio;
    public audio [] sfxAudio;
    public bool Moving;
    public bool pausing;

    public bool bgmEnable;
    public int sfxStats;
    public int musicStats;
   
    private float startVolume;
    private float startVolumeBGM1;
    void Awake()
    {
        musicStats = PlayerPrefs.GetInt("musicStats");
        if (musicStats == 0)
        {
            bgmEnable = true;
        }
        else
        {
            bgmEnable = false;
        }

        if (_Audio == null)
        {
            DontDestroyOnLoad(this);
            _Audio = this;
        }
        else
        {
            if (_Audio != this )
            {
                Destroy(this);
            }
        }
       
        
        foreach (var bgm in bgmAudio)
        {
            bgm.audioSource = gameObject.AddComponent<AudioSource>();
            bgm.audioSource.clip = bgm.clip;
            bgm.audioSource.volume = bgm.volume;
            bgm.audioSource.pitch = bgm.pitch;
            bgm.audioSource.loop = bgm.loop;
        }

        foreach (var sfx in sfxAudio)
        {
            sfx.audioSource = gameObject.AddComponent<AudioSource>();
            sfx.audioSource.clip = sfx.clip;
            sfx.audioSource.volume = sfx.volume;
            sfx.audioSource.pitch = sfx.pitch;
            sfx.audioSource.loop = sfx.loop;
        }
    }

    private void Start()
    {
        playBGM(0);
        playBGM(1);
        playBGM(2);
        playBGM(3);
        playBGM(4);
        playBGM(5);
        playBGM(6);
        playBGM(7);
        playBGM(8);

        sfxStats = PlayerPrefs.GetInt("sfxStats");
    }

    public void playBGM(int index)
    {
        bgmAudio[index].audioSource.Play();
        
    }
    public void playSFX(int index)
    {
        if (sfxStats == 0)
        {
            sfxAudio[index].audioSource.Play();
        }
    
    }
    public void stopBGM(int index)
    {
        bgmAudio[index].audioSource.volume = 0 ;
        
    }
    public void stopSFX(int index)
    {
        sfxAudio[index].audioSource.Stop();
        
    }
   
    public  IEnumerator FadeIn(int index,float FadeTime) {
       
        startVolume = sfxAudio[index].audioSource.volume;
        while (true) {
             startVolume += Time.deltaTime / FadeTime;
             sfxAudio[index].audioSource.volume = startVolume;
			if(startVolume >= .3f && !Moving){
				
				break;
			}
            yield return null;
        }
    }

    public IEnumerator FadeOutA(int index, float FadeTime)
    {
        startVolume = sfxAudio[index].audioSource.volume;
        while (true)
        {
            startVolume -= Time.deltaTime / FadeTime;
            sfxAudio[index].audioSource.volume = startVolume;
			if(startVolume <= 0 || Moving){

                break;
			}
            yield return null;
        }
    }

    public IEnumerator FadeInBGM(int index, float FadeTime, float target)
    {
        float startVolumeBGM0;
        
        startVolumeBGM0 = bgmAudio[index].audioSource.volume;
        while (bgmEnable)
        {
            startVolumeBGM0 += Time.deltaTime / FadeTime;
            bgmAudio[index].audioSource.volume = startVolumeBGM0;
            if (startVolumeBGM0 >= target)
            {
               
                break;
            }
            yield return null;
        }
    }

    public IEnumerator FadeOutBGM(int index, float FadeTime)
    {
        float startVolumeBGM0;
       
        startVolumeBGM0 = bgmAudio[index].audioSource.volume;
        while (bgmEnable)
        {

            startVolumeBGM0 -= Time.deltaTime / FadeTime;
            bgmAudio[index].audioSource.volume = startVolumeBGM0;
            if (startVolumeBGM0 <= 0)
            {
               
                break;
            }
            yield return null;
        }

    }

    public IEnumerator pitchInBGM(int index, float FadeTime)
    {
        float startPitch;

        startPitch = bgmAudio[index].audioSource.pitch;
        while (true)
        {

            startPitch += Time.deltaTime / FadeTime;
            bgmAudio[index].audioSource.pitch = startPitch;
            if (startPitch >= 1f)
            {
              
                break;
            }
            yield return null;
        }

    }

    public IEnumerator pitchOutBGM(int index, float FadeTime)
    {
        float startPitch;

        startPitch = bgmAudio[index].audioSource.pitch;
        while (true)
        {

            startPitch -= Time.deltaTime / FadeTime;
            bgmAudio[index].audioSource.pitch = startPitch;
            if (startPitch <= 0f)
            {
                break;
            }
            yield return null;
        }

    }

    public IEnumerator pausingSound()
    {
        for (int i = 1; i < bgmAudio.Length; i++)
        {

            StartCoroutine(FadeOutBGM(i, 5f));
            yield return null;

        }

    }

    public IEnumerator unpausingSound()
    {
        for (int i = 1; i < bgmAudio.Length; i++)
        {

            StartCoroutine(FadeInBGM(i, 5f,.6f));
            yield return null;

        }

    }
    public IEnumerator recapSound()
    {
        for (int i = 1; i < bgmAudio.Length; i++)
        {
            if (i==3)
            {

            }
            else
            {
                StartCoroutine(FadeOutBGM(i, 5f));
            }
            
            yield return null;

        }

    }

    public void stopWalk()
    {

        sfxAudio[9].audioSource.volume = 0;
    }
}

  

