﻿using System;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class gameManager : MonoBehaviour
{
    public int level = 1;
    public bool gameOver;
    public bool playerGoal;
    public Image overlay;
    public GameObject pauseMenu;
    public FloatingJoystick _Joystick;
    public Transform outline;
    public Transform key;
    public Vector2[] keyPost;
    public Vector3[] targetEnding1;
    public cameraManager _CameraManager;

    public Slider _slider;
    public TextMeshProUGUI timerText;
    //fade in fade out
    private Color startColor;
    private Color targetColor;
    private int _resetScene;
    private WaitForFixedUpdate _waitForFixedUpdate;
    private WaitForSeconds _forSeconds;
    private WaitForSeconds _forSecondsEnd;

    //Time target

    public timeTarget[] _timesTarget;

    //animator slider
    public Animator[] _animatorObjective;
    public Animator[] _animatorStar;
    private string anim1, animationName;
    public TextMeshProUGUI objective, objective1;
    //end animator slider

    //   private adsManager _adsManager;

    [SerializeField] private Color target;
    [SerializeField] private Color target1;
    [SerializeField] private Animator pauseAnimator;
    [SerializeField] private Vector3[] playerPost;
    [SerializeField] private GameObject[] levels;
    [SerializeField] private Transform player;
    [SerializeField] private TrailRenderer playerTrail;
    public bool once = true;
    public bool pause = false;
    public bool starting = true;
    private float timing;

    //guiding light
    private int countLight = 0;
    private WaitForSeconds guideDelay;
    public GuidingLigts[] guidingLightsList;
    [HideInInspector] public bool guidingLightTggl;
    
    //only level 3

    private int guideL3 = 10;
    
    // only level 6
    private int guideL61 = 12;
    private int guideL62 = 17;
    private void Start()
    {
       
        // TIMER  timer = 0;
        anim1 = "play";
        animationName = "objective";
        timerTic._timer.setAnimator(_animatorStar);
        if (Application.platform == RuntimePlatform.Android)
        {
            adsManager._Ads.hideDestroyBanner(true);
            // adsManager._Ads.clearInterstitial();
            adsManager._Ads.RequestInterstitial();
        }
        //timerTic
        timerTic._timer.setSliderandText(_slider,timerText);
      
        //end timerTic
        guideDelay = new WaitForSeconds(.2f);
        if ( PlayerPrefs.GetInt("level") != 0)
        {
            level = PlayerPrefs.GetInt("level");
        }
        //recap start
        recapScore._recap.setIndex(level - 1);

        //animatorObjective
        objectiveAnimationIn();
        
        // _adsManager = GetComponent<adsManager>();
        _waitForFixedUpdate = new WaitForFixedUpdate();
        _forSeconds = new WaitForSeconds(2);
        _forSecondsEnd = new WaitForSeconds(5.5f);
        StartCoroutine(fading(true));
        _resetScene = SceneManager.GetActiveScene().buildIndex;
        player.position = playerPost[level-1];
        setLevelMap();
        key.position = keyPost[level - 1];
        outline.position = keyPost[level - 1];
        objective.text = timerTic._timer.timeConvert((int)_timesTarget[level - 1]._timesTarget[0]) ;
        objective1.text = timerTic._timer.timeConvert((int)_timesTarget[level - 1]._timesTarget[1]);
        timerTic._timer.setTargetTime((int)_timesTarget[level - 1]._timesTarget[0]);
        timerTic._timer.setTargetTime1((int)_timesTarget[level - 1]._timesTarget[1]);
        timerTic._timer.resetTimer();
        key.gameObject.SetActive(true);
        StartCoroutine(audioManager._Audio.FadeInBGM(0, 10,.6f));
        StartCoroutine(audioManager._Audio.FadeInBGM(1, 10,.6f));
        StartCoroutine(audioManager._Audio.FadeInBGM(2, 10,.6f));
        StartCoroutine(audioManager._Audio.FadeInBGM(3, 10,.6f));
        StartCoroutine(audioManager._Audio.FadeInBGM(4, 10,.6f));
        StartCoroutine(audioManager._Audio.FadeInBGM(8, 10,.6f));
      

        //StartCoroutine(guideLight());
    }

    private void Update()
    {
        /* TIMER if (!starting)
        {
            timer += Time.deltaTime;
        }*/
        
        if (gameOver &&!once)
        {
            // _adsManager.GameOver();
          
       
            StartCoroutine(fading(false));
           
            once = true;
        }

        if (Input.GetKey(KeyCode.Escape) && !pause && !once&&!playerGoal&&player.gameObject.activeInHierarchy && !starting)
        {
            audioManager._Audio.playSFX(11);
            // StartCoroutine(audioManager._Audio.FadeOutBGM(2, 20));
            //StartCoroutine(audioManager._Audio.pitchOutBGM(2,.5f));
            // TIMER timerText.transform.SetAsLastSibling();
            StartCoroutine(audioManager._Audio.pausingSound());
            pauseAnimator.SetFloat("upause",0);
            objectiveAnimationIn();
            once = true;
			
            StartCoroutine(fading(false));
            pause = true;
            audioManager._Audio.pausing = pause;
        }
        if (Input.GetKey(KeyCode.Escape) && pause && !once && !playerGoal)
        {
            // TIMER timerText.transform.SetAsFirstSibling();
            audioManager._Audio.playSFX(11);
          //  StartCoroutine(audioManager._Audio.pitchInBGM(2, .5f));
            StartCoroutine(audioManager._Audio.unpausingSound());
            pauseAnimator.SetFloat("upause",1);
            objectiveAnimationOut();
           
           once = true;          
            StartCoroutine(fading(true));
        }
    }

    public IEnumerator fading(bool fadeIn)
    {
        timing = 0;
        if (playerGoal)
        {
            _Joystick.gameObject.SetActive(false);
            timerTic._timer.stopCounter();
            recapScore._recap.setTime(timerTic._timer.getTime());
            overlay.gameObject.SetActive(true);
            StartCoroutine(playerEnding());
            StartCoroutine(audioManager._Audio.pausingSound());
            StartCoroutine(_CameraManager.cameraMoveManager(true,4));
            yield return _forSecondsEnd;
        }

        if (once && !playerGoal)
        {
           
            if (pause)
            {
                StartCoroutine(_CameraManager.cameraMoveManager(false,2));
            }
            else
            {
                StartCoroutine(_CameraManager.cameraMoveManager(true,2));
            }
        }
        if (fadeIn)
        {
            
            startColor = overlay.color;
            targetColor = target;
            if (!once && !playerGoal)
            {
                yield return _forSeconds;
            }
           
        }
        else
        {
            overlay.gameObject.SetActive(true);
            targetColor = target1;
            startColor = overlay.color;
            //Debug.Log(startColor);
        }

        
      
        while (true)
        {
            
            timing += Time.deltaTime ;
            overlay.color = Color.Lerp(startColor, targetColor, timing);

            if (overlay.color.a <= 0 && fadeIn)
            {
               
                if (!once || starting)
                {
                    timerTic._timer.startCounter();
                   
                    playerTrail.emitting = true;
                    starting = false;
                    objectiveAnimationOut();
                    once = false;
                   
                }
                else
                {
                  
                    pause = false;
                    audioManager._Audio.pausing = pause;

                }
                pauseMenu.SetActive(false);
                overlay.gameObject.SetActive(false);
               
                
                break;
                
               
            }

            if (overlay.color.a >= .5f && !fadeIn && pause)
            {
                pauseMenu.SetActive(true);
               
                break;
            }
            if (overlay.color.a >= 1 && !fadeIn)
            {
                if (playerGoal && !pause)
                {
                    /*  //demo
                      if (level == 2)
                      {
                          SceneManager.LoadScene(0);
                      }
                      //demo up
                      else
                      {
                      if (level == 6)
                      {
                          SceneManager.LoadScene(0);
                      }*/

                    if (level > PlayerPrefs.GetInt("levelUnlock"))
                    {
                        PlayerPrefs.SetInt("levelUnlock", level);
                    }
                    
                    timerTic._timer.stopCounter();
                    recapScore._recap.countStar((int)_timesTarget[level - 1]._timesTarget[0], (int)_timesTarget[level - 1]._timesTarget[1]);
                    StartCoroutine(audioManager._Audio.FadeOutBGM(0, 10));

                    SceneManager.LoadScene(4);
                    //  SceneManager.LoadScene(3);

                    //}


                }
                else
                {
                    timerTic._timer.stopCounter();
                    timerTic._timer.resetTimer();
                    if (Application.platform == RuntimePlatform.Android)
                    {
                        adsManager._Ads.showInterstitial();
                    }
                    SceneManager.LoadScene(_resetScene);
                }
                
                break;
            }

            yield return _waitForFixedUpdate;
        }

    }


    private void setLevelMap()
    {
        levels[level-1].SetActive(true);

    }

    public void mainMenuBrr()
    {

        // TIMER PlayerPrefs.SetFloat("time", timer);
        
        timerTic._timer.stopCounter();
        
        audioManager._Audio.playSFX(12);
       
         SceneManager.LoadScene(1);
        //SceneManager.LoadScene(0);
    }
    public void restartBtt()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            adsManager._Ads.showInterstitial();
        }
        audioManager._Audio.playSFX(12);
        timerTic._timer.stopCounter();
        SceneManager.LoadScene(_resetScene);
    }

    public IEnumerator guideLight()
    {
        
        while (guidingLightTggl)
        {
           
            if (countLight <= ( guidingLightsList[level-1].guidingLights.Length-1))
            {
                guidingLightsList[level-1].guidingLights[countLight].SetActive(true);
            }

            if (countLight >= 12 && level != 3 && level != 6)
            {
                guidingLightsList[level-1].guidingLights[countLight-12].SetActive(false);
                
            }
            else if(countLight >= 12 && level == 3 )
            {
                if (countLight - 12 >= 7 && countLight - 12 < 11)
                {
                    guidingLightsList[level-1].guidingLights[guideL3].SetActive(false);
                    guideL3--;

                }
                else
                {
                    guidingLightsList[level-1].guidingLights[countLight-12].SetActive(false);
                }
               
            }
            else if(countLight >= 12 && level == 6 )
            {
                if (countLight - 12 >= 8 && countLight - 12 < 13)
                {
                    guidingLightsList[level-1].guidingLights[guideL61].SetActive(false);
                    guideL61--;

                }
                else if (countLight - 12 >= 13 && countLight - 12 < 18)
                {
                    guidingLightsList[level-1].guidingLights[guideL62].SetActive(false);
                    guideL62--;

                }
                else
                {
                    guidingLightsList[level-1].guidingLights[countLight-12].SetActive(false);
                }
               
            }

            
            if (countLight == guidingLightsList[level-1].guidingLights.Length + 11)
            {
                countLight = 0;
                if (level == 3)
                {
                    guideL3 = 10;
                }
                if (level == 6)
                {
                    guideL61 = 12;
                    guideL62 = 17;
                }
                
            }
            else
            {
                countLight++;
            }

            
            yield return guideDelay;
        }
        countLight = 0;
        foreach (var guide in guidingLightsList[level-1].guidingLights)
        {
            guide.SetActive(false);
        }
    }

    /* TIMER private void timeConvert()
    {
        second = (int)timer;
        if (second >= 60f)
        {

            minute = (second / 60);
            second = second % 60;

            if (minute >= 60)
            {

                hour = (minute / 60);
                minute = minute % 60;

            }

        }
        timerText.text = hour.ToString("00") + ":" + minute.ToString("00") + ":" + second.ToString("00");

    }*/

    public IEnumerator playerEnding()
    {
        
        while (true)
        {
          player.position = Vector3.MoveTowards(player.position,targetEnding1[level-1],3* Time.deltaTime);

          if (player.position == targetEnding1[level-1] )
          {
              break;
          }
          yield return null;
        }

        
    }

    public void objectiveAnimationIn()
    {
        int i = 1;
        foreach(var _animator in _animatorObjective)
        {
            _animator.SetFloat(anim1, 1);
            _animator.Play(animationName + i.ToString(),-1,0);
            i++;
        }


       /* _animatorSlider.SetFloat(anim1, 1);
        _animatorSlider.Play(animationName, -1,0);*/
    }

    public void objectiveAnimationOut()
    {
        int i = 1;
        foreach (var _animator in _animatorObjective)
        {
            _animator.SetFloat(anim1, -1);
            _animator.Play(animationName + i.ToString(), -1, 1);
            i++;
        }
        /*  _animatorSlider.Play(animationName, -1, 1);
          _animatorSlider.SetFloat(anim1, -1);*/
    }
}
