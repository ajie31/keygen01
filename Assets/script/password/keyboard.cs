﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class keyboard : MonoBehaviour
{
    private string input;
    private string[] textInputs;
    
    [SerializeField] private TextMeshProUGUI attemptCountText;
    [SerializeField] private TextMeshProUGUI[] printTextInputs;
    [SerializeField] private TextMeshProUGUI timerText;
    [SerializeField] private GameObject lowCase;
    [SerializeField] private GameObject upperCase;
    
    [SerializeField] private RectTransform player;
    [SerializeField] private GameObject deathParticle;
    private bool shift = false;
    private int indexText = 0;
    private int attempt = 5;
    private string answer;
    private Camera mCamera;
    
    //fading
    private float delayStart;
   
    public Image overlay;
    public GameObject welldone;
    public Color target;
    private Color startColor;
    private WaitForSeconds _forSeconds;
    private WaitForFixedUpdate _waitForFixedUpdate;
    //TIMER
    private float timer;
    private int second;
    private int minute;
    private int hours;

    private void Start()
    {
        StartCoroutine(audioManager._Audio.FadeInBGM(2, 10, .1f));
        // timer = PlayerPrefs.GetFloat("timer");
        _waitForFixedUpdate = new WaitForFixedUpdate();
        StartCoroutine(audioManager._Audio.FadeOutBGM(7, 5f));
        StartCoroutine(audioManager._Audio.FadeOutBGM(6, 5f));
        _forSeconds = new WaitForSeconds(2);
        textInputs = new string[6];
        attemptCountText.text = attempt.ToString();
        mCamera = Camera.main;
        deathParticle.transform.position =  mCamera.ScreenToWorldPoint(player.position) + new Vector3(0,0,10);
        StartCoroutine(fading(true));
    }
    private void Update()
    {
        timer += Time.deltaTime;
    }
    public void btnKeyboard(Button btn){
        if (indexText <= 5)
        {
			audioManager._Audio.playSFX(13);
            input = btn.name;
            textInputs[indexText] = input;
            printTextInputs[indexText].text = input;
          

            if (indexText == 5)
            {

                StartCoroutine(letterCheck());
            }
            indexText++;
            
           
        }


    }

    IEnumerator letterCheck()
    {
        foreach (var textInput in textInputs)
        {
            answer += textInput;
        }

        yield return null;
        if (answer == "SPySek")
        {

         

            //audioManager._Audio.playSFX(10);
            audioManager._Audio.playSFX(28);

            StartCoroutine(fading(false));
        }
        else
        {
            audioManager._Audio.playSFX(14);
          
            for (int i = 0; i < printTextInputs.Length; i++)
            {
                printTextInputs[i].text = "";
            }
            for (int i = 0; i < textInputs.Length; i++)
            {
                textInputs[i] = null;
            }
            indexText = 0;
            crackAttempt();

        }
        
        
        
 
    }

    private void timeConvert()
    {
        second = (int)timer;
        if (second >= 60f)
        {

            minute = (second / 60);
            second = second % 60;

            if (minute >= 60)
            {

                hours = (minute / 60);
                minute = minute % 60;

            }

        }



    }

    public void showTimer()
    {

        timeConvert();
        timerText.text = hours.ToString("00") + ":" + minute.ToString("00") + ":" + second.ToString("00");

    }

    public void shiftBttn()
    {
        if (shift)
        {
            lowCase.SetActive(true);
            upperCase.SetActive(false);
            shift = false;
        }
        else
        {
            lowCase.SetActive(false);
            upperCase.SetActive(true);
            shift = true;
        }
        
        
    }

    public void backSpace()
    {
        if (indexText>0)
        {
            indexText -= 1;
            textInputs[indexText] = null;
            printTextInputs[indexText].text = "-";
        }
       
        
    }

    private void crackAttempt()
    {
		
        attempt -= 1;
        attemptCountText.text = attempt.ToString();
        if (attempt == 0)
        {
			audioManager._Audio.playSFX(7);
            StartCoroutine(fading(false));
           player.gameObject.SetActive(false);
		   
           deathParticle.SetActive(true);
            //SceneManager.LoadScene(1);
        }
        
    }
    
    public IEnumerator fading(bool fadeIn)
    {
        delayStart = 0;
        if (fadeIn)
        {
            startColor = overlay.color;
            yield return _forSeconds;
        }
        else
        {
            overlay.gameObject.SetActive(true);
            target = startColor;
            startColor = overlay.color;
            //Debug.Log(startColor);
        }

      
        while (true)
        {
            delayStart += Time.deltaTime;
            overlay.color = Color.Lerp(startColor, target, delayStart);

            if (overlay.color.a <= 0 && fadeIn)
            {
              
                
                overlay.gameObject.SetActive(false);
                break;
            }
            if (overlay.color.a >= 1 && !fadeIn)
            {
                if (attempt == 0)
                {
                    //SceneManager.LoadScene(0);
                    SceneManager.LoadScene(1);
                }
                else
                {
                    welldone.SetActive(true);
                   // showTimer();
                   // timerText.gameObject.SetActive(true);
                    
                }
                
                break;
            }

            yield return _waitForFixedUpdate;
        }

    }
}
