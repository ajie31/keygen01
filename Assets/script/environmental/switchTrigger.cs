﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class switchTrigger : MonoBehaviour
{
    public bool activeSwitch;
    public Animator animationTriggered;
    public bool activateSwitch;

    public Transform switchTr;
    
    public bool toRight;
    public bool toBottom;
    public bool toLeft;
    public bool toUp;
    
    public bool fromRight;
    public bool fromBottom;
    public bool fromLeft;
    public bool fromUp;

    public int rotateTarget;
    
    public Vector3 targetObs;
    private switchCable _switchCable;

    private bool recentStatus;
    private string animatorId;
    private SpriteRenderer _renderer;

    private CircleCollider2D _collider2D;

    private Quaternion deltaRotation;

    private Vector3 eulerAngle;

    private WaitForFixedUpdate waitFu;
    // Start is called before the first frame update
    void Start()
    {
        eulerAngle = new Vector3(0,0,9);
        recentStatus = activeSwitch;
        _renderer = GetComponent<SpriteRenderer>();
        _collider2D = GetComponent<CircleCollider2D>();
        _switchCable = switchTr.GetComponent<switchCable>();
        animatorId = "playTrigger";
        //start state
       
        _renderer.color = environmentStatic.current.getColor(false);
        _collider2D.enabled = false;
        activateSwitch = false;
        
    }

    // Update is called once per frame
    void Update()
    {
        if ((recentStatus != activeSwitch) )
        {
            if (!activeSwitch)
            {
                animationTriggered.Play("icon",-1,0f);
                animationTriggered.SetFloat(animatorId,1);
                recentStatus = activeSwitch;
                _renderer.color = environmentStatic.current.getColor(false);
                _collider2D.enabled = false;
                activateSwitch = false;
            }
            else
            {
              
                recentStatus = activeSwitch;
                _renderer.color = environmentStatic.current.getColor(true);
                _collider2D.enabled = true;
            }

            
          
        }

        if (activateSwitch)
        {
            if (toRight)
            {
                StartCoroutine(rotateSwitchR());
                
            }

            if (toLeft)
            {
                // StartCoroutine(rotateSwitchL());
            }

            if (fromBottom)
            {
                StartCoroutine(_switchCable.deactiveBottom());
            }

            activateSwitch = false;
        }

       
    }
    private IEnumerator rotateSwitchR()
    {
        while (true)
        {
            if ((int)(switchTr.eulerAngles.z) <= rotateTarget)
            {
                deltaRotation = Quaternion.Euler(eulerAngle*(10 * Time.deltaTime));
                switchTr.rotation = switchTr.rotation * deltaRotation;
            }

            else
            {
                if (_switchCable.level == 3)
                {
                    StartCoroutine(_switchCable.activateObsLv3());
                }

                StartCoroutine(_switchCable.activateRight());
                
                break;
            }
            
            yield return waitFu;
        }
            
        
    }
  /*  private IEnumerator rotateSwitchL()
    {
        while (true)
        {
            if ((int)(switchTr.eulerAngles.z) != rotateTarget)
            {
                deltaRotation = Quaternion.Euler(eulerAngle*(10 * -Time.fixedDeltaTime));
                switchTr.rotation = switchTr.rotation * deltaRotation;
                
            }

            else
            {
                if (_switchCable.level == 6)
                {
                    StartCoroutine(_switchCable.activateObsLv6());
                }

                
                StartCoroutine(_switchCable.activateRight());
                
                break;
            }
            
            yield return waitFu;
        }
            
        
    }*/
}
