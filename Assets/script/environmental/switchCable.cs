﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class switchCable : MonoBehaviour
{
    [SerializeField] private gameManager gm;
    public bool powerSwitch;

    private bool recentStatusSwitch;
    //bottom
    public switchTrigger[] triggersB;
    public singleObs[] SingleObsesB;
    public SpriteRenderer[] cablesB;
    // end of bottom
    
    //right
    // public switchTrigger[] triggersR;
    public obsMove[] ObsMovesR;
    public singleObs[] SingleObsesR;
    public SpriteRenderer[] cablesR;

    public doorTrigger[] doorsR;
    //end of right
    public bool Right;
    public bool Bottom;
    public bool Left;
    public bool Up;
 
  
    public bool singleUse;
    
   // private WaitForFixedUpdate waitFU;

    private SpriteRenderer switchSprite;
    private SpriteRenderer switchLogo;

    public int level;

    private WaitForSeconds _waitForSeconds;
    
    // obs level 5
    private int countObs;
    private float timing;
    private WaitForFixedUpdate _fixedUpdate;

    
    // Start is called before the first frame update
    void Start()
    {
        recentStatusSwitch = powerSwitch;
        switchSprite = GetComponent<SpriteRenderer>();
        switchLogo = transform.GetChild(0).GetComponent<SpriteRenderer>();
        level = gm.level;

        _waitForSeconds = new WaitForSeconds(2.1f);
        StartCoroutine(deactiveBottom());
        StartCoroutine(deactiveRight());
        _fixedUpdate = new WaitForFixedUpdate();
        //start state
        switchSprite.color = environmentStatic.current.getColor(false);
        switchLogo.color = environmentStatic.current.getColorWD(false);
        
        timing = 5.16f;
        if (Bottom)
        {
            
            StartCoroutine(deactiveBottom());
        }

        if (Right)
        {
            StartCoroutine(deactiveRight());
        }

       
        StartCoroutine(switchActFirst());
            
    }

    // Update is called once per frame
    void Update()
    {
       
        if ((recentStatusSwitch != powerSwitch))
        {
            
            if (!powerSwitch)
            {
                switchSprite.color = environmentStatic.current.getColor(false);
                switchLogo.color = environmentStatic.current.getColorWD(false);
            
                    StartCoroutine(deactiveBottom());
               
                    StartCoroutine(deactiveRight());
         
            }
            else
            {
                switchSprite.color = environmentStatic.current.getColor(true);
                switchLogo.color = Color.white;
                if (Bottom)
                {
                    StartCoroutine(activateBottom());
                    
                }

                if (Right)
                {
                    StartCoroutine(activateRight());
                }
            }
            recentStatusSwitch = powerSwitch;
           
        }
    }
//right
    public IEnumerator activateRight()
    {
        
        foreach (var cable in cablesR)
        {
            //cable.SetActive(true);
            
            cable.color = environmentStatic.current.getColorSwitch(true);
            if (level == 3)
            {
                cable.transform.GetChild(0).GetComponent<SpriteRenderer>().color = environmentStatic.current.getColorSwitch(true);
            }
        }

        yield return null;
       
        foreach (var trigger in ObsMovesR)
        {
                trigger.activateTriggerObs = true;
        }

        yield return null;
        foreach (var door in doorsR)
        {
            door.activate = true;
        }

        yield return null;

        if (level != 3)
        {
            foreach (var singleObse in SingleObsesR)
            {
                singleObse.activeSingle= true;
            }
            yield return null;
        }
       
    }
    public IEnumerator deactiveRight()
    {

        foreach (var cable in cablesR)
        {
           // cable.SetActive(false);
           cable.color = environmentStatic.current.getColorSwitch(false);
           if (level == 3)
           {
               cable.transform.GetChild(0).GetComponent<SpriteRenderer>().color = environmentStatic.current.getColorSwitch(false);
           }
        }

        yield return null;

        foreach (var trigger in ObsMovesR)
        {
            trigger.activateTriggerObs = false;
        }
        yield return null;
        if (level != 3)
        {
            
            foreach (var singleObse in SingleObsesR)
            {
                singleObse.activeSingle= false;
            }
        }

        yield return null;
    }
    
    //end of right
    
    //bottom
    public IEnumerator deactiveBottom()
    {
        foreach (var trigger in triggersB)
        {
            trigger.activeSwitch = false;

        }
        yield return null;
        
        foreach (var cable in cablesB)
        {
            //cable.SetActive(false);
            cable.color = environmentStatic.current.getColorSwitch(false);
            cable.sortingOrder = 1;

        }

        yield return null;
        if (level != 3||powerSwitch)
        {
            foreach (var single in SingleObsesB)
            {
                single.activeSingle = false;
            }
            yield return null;
        }
      

        if ((level == 3) && powerSwitch)
        {
            SingleObsesB[0].transform.rotation = Quaternion.Euler(0,0,45);
        }
        if ((level == 5) && powerSwitch)
        {
            SingleObsesB[0].transform.rotation = Quaternion.Euler(0,0,45);
            SingleObsesB[1].transform.rotation = Quaternion.Euler(0,0,45);
            SingleObsesB[2].transform.rotation = Quaternion.Euler(0,0,45);
        }


      
            
    }
    public IEnumerator activateBottom()
    {

        foreach (var cable in cablesB)
        {
            //cable.SetActive(true);
            cable.color = environmentStatic.current.getColorSwitch(true);
            cable.sortingOrder = 3;
        }

        yield return null;

        foreach (var single in SingleObsesB)
        {
            single.activeSingle = true;
        }
        yield return null;

        foreach (var trigger in triggersB)
        {
            trigger.activeSwitch = true;

        }
        yield return null;
        
    }

    public IEnumerator activateObsLv3()
    {

        
                
        SingleObsesR[0].activeSingle = true;
        SingleObsesR[2].activeSingle = true;
               
        yield return _waitForSeconds;
               // timing -= Time.deltaTime;
        SingleObsesR[1].activeSingle = true;
               

    }

    private IEnumerator switchActFirst()
    {
        
        yield return null;
        level = gm.level;
       
           powerSwitch = true;
        
        
    }
    
  /*  public IEnumerator activateObsLv6()
    {
        
        while (true)
        {
            
            timing -= Time.deltaTime;


            if (timing <= 5.16- (.86f * countObs) && timing >= 0)
            {
                SingleObsesR[countObs].activeSingle = true;
                countObs++;

            }
            else if (timing <= 0)
            {
                
                // _collider2D.enabled = true;
                countObs = 0;
                timing = 5.16f;
                
                //  activateObs = false;
            }

            yield return _fixedUpdate;
        }
        
    }*/
    //end of bottom
}
