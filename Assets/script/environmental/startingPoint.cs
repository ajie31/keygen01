﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class startingPoint : MonoBehaviour
{
    public bool activeStart;
    public bool doorObs;
    public gameManager _GameManager;
    private Animator animStart;
    
   
    private bool currentStat;
  
    
    // obs trigger by start
    private float timing = 2.5f; 
    private bool once;
    private string animatorId;
    public Vector3 target; 
    public singleObs[] SingleObses;
    //end of obs
    
    // Start is called before the first frame update
    void Start()
    {
        animStart = GetComponent<Animator>();
        currentStat = activeStart;
        transform.GetChild(0).gameObject.SetActive(false);
        animatorId = "starterAct";
    }

    // Update is called once per frame
    void Update()
    {
        if ((currentStat!=activeStart))
        {
           
            if (activeStart)
            {
                
                transform.GetChild(0).gameObject.SetActive(true);
                animStart.SetFloat(animatorId,1);
                animStart.Play("starter",-1,0f);
                audioManager._Audio.playSFX(0);
                audioManager._Audio.playSFX(2);

            }
            else
            {
              
                transform.GetChild(0).gameObject.SetActive(false);
                //_Trail.time = 1000;
                animStart.SetFloat(animatorId,-1);
                audioManager._Audio.stopSFX(2);
                audioManager._Audio.playSFX(1);
               
                
            }

            currentStat = activeStart;
        }

        if (activeStart && doorObs)
        {
            
               /* if (!once)
                {
                    SingleObses[1].activeSingle = true;
                    SingleObses[3].activeSingle = true;
                    once = true;
                }
                timing -= Time.deltaTime;
                if (timing <= 0)
                {
                    SingleObses[0].activeSingle = true;
                    SingleObses[2].activeSingle = true;
                    SingleObses[4].activeSingle = true;
                    timing = 2.5f;
                    once = false;
                    doorObs = false;
                }*/
            //Debug.Log("here starting");
            if (_GameManager.level == 2)
            {
                SingleObses[0].activeSingle = true;
                SingleObses[2].activeSingle = true;
                SingleObses[4].activeSingle = true;
                SingleObses[1].activeSingle = true;
                SingleObses[3].activeSingle = true;
                doorObs = false;
            }

            if (_GameManager.level == 4)
            {
                 SingleObses[0].activeSingle = true;
                 doorObs = false;
            }
          
            
        }
        
    }


}
