﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class obsMove : MonoBehaviour
{
    public bool activateTriggerObs;
    public bool activateObs;
    //public bool powerOn;
   
    public Transform[] obstacles;
    public Vector3 target;
    public Animator animationTriggered;
    public bool ordinaryTriangle;
    public bool obsOnceOpen;
    public float delayOnceOpen;
    public bool rotationThis;
    public bool delayedObs;
    private string animatorId;
    private SpriteRenderer _renderer;
    private SpriteRenderer _rendererWire;
    private WaitForFixedUpdate _waitForFixedUpdate;
    private bool currentStat;
    private bool currentPower;
    private CircleCollider2D _collider2D;
    private int countObs = 0;
    private int currentObs = 0;
    private float timing = 6.02f; 
    private bool reverse = false;
    private Vector3[] targetPos;
    private Vector3[] recoverPos;

    private bool once = false;
  
    // Start is called before the first frame update
    void Start()
    {
        //Debug.Log(obstacles[0].localPosition + target);
        //currentPower = powerOn;
        animatorId = "playTrigger";
        currentStat = activateTriggerObs;
        _collider2D = GetComponent<CircleCollider2D>();
        if (ordinaryTriangle||obsOnceOpen ||delayedObs)
        {
            if (obsOnceOpen || delayedObs)
            {
                timing = delayOnceOpen;
            }

            if (!delayedObs)
            {
                targetPos = new Vector3[obstacles.Length];
                recoverPos = new Vector3[obstacles.Length];
                for (int i = 0; i < obstacles.Length; i++)
                {
                    recoverPos[i] = obstacles[i].localPosition;
                    targetPos[i] = obstacles[i].localPosition + target;
                }
            }
            
        }
        

        _renderer = GetComponent<SpriteRenderer>();
        _rendererWire = transform.GetChild(2).GetComponent<SpriteRenderer>();
        
        //start state
        
        currentStat = activateTriggerObs;
        _collider2D.enabled = false;
        _renderer.color = environmentStatic.current.getColor(activateTriggerObs);
        _rendererWire.color = environmentStatic.current.getColorWD(activateTriggerObs);
    }

    // Update is called once per frame

    private void FixedUpdate()
    {
        if ((currentStat != activateTriggerObs) )
        {
            if (!activateTriggerObs)
            {
                animationTriggered.Play("icon",-1,0f);
                animationTriggered.SetFloat(animatorId,1);
                currentStat = activateTriggerObs;
                _collider2D.enabled = false;
                _renderer.color = environmentStatic.current.getColor(activateTriggerObs);
                _rendererWire.color = environmentStatic.current.getColorWD(activateTriggerObs);
            }
            else
            {
               
                currentStat = activateTriggerObs;
                _collider2D.enabled = true;
                _renderer.color = environmentStatic.current.getColor(activateTriggerObs);
                _rendererWire.color = environmentStatic.current.getColorWD(activateTriggerObs);
            }

            
        }
        if (activateObs && activateTriggerObs && (ordinaryTriangle ||rotationThis||obsOnceOpen||delayedObs))
        {
            _collider2D.enabled = false;
            activateTriggerObs = false;
            _renderer.color = environmentStatic.current.getColor(activateTriggerObs);
        }
        if (activateObs)
        {
            if (ordinaryTriangle)
            {
               

                /*  if ((currentPower != powerOn) )
                  {
                      if (!powerOn)
                      {
                          countObs = 0;
                      }
      
                      currentPower = powerOn;
                  }*/

                if (!reverse)
                {
                    if (obstacles[countObs].localPosition.x < targetPos[0].x)
                    {
                        obstacles[countObs].localPosition =
                            Vector3.MoveTowards(obstacles[countObs].localPosition, targetPos[countObs],
                                6 * Time.deltaTime);
                    }
                    else
                    {
                        if (countObs != obstacles.Length - 1)
                        {
                            countObs++;
                        }
                        else
                        {
                            reverse = true;
                            countObs = 0;
                        }
                    }
                }

                if (reverse)
                {
                    if (obstacles[countObs].localPosition.x > recoverPos[0].x)
                    {
                        obstacles[countObs].localPosition =
                            Vector3.MoveTowards(obstacles[countObs].localPosition, recoverPos[countObs],
                                6 * Time.deltaTime);
                    }
                    else
                    {
                        if (countObs != obstacles.Length - 1)
                        {
                            countObs++;
                        }
                        else
                        {
                            reverse = false;
                            countObs = 0;
                        }
                    }
                }

            }
            
            if (obsOnceOpen)
            {
               // Debug.Log("here");
                timing -= Time.deltaTime;


                if (timing <= delayOnceOpen- (.86f * countObs) && timing >= 0)
                {
                    obstacles[countObs].GetComponent<singleObs>().activeSingle = true;
                    countObs++;

                }
                else if (timing <= 0)
                {
                   // _collider2D.enabled = true;
                    countObs = 0;
                    timing = delayOnceOpen;
                  //  activateObs = false;
                }

            }

            if (rotationThis)
            {
                foreach (var obs in obstacles)
                {
                    obs.GetComponent<singleObs>().activeSingle = true;
                }

                activateObs = false;
            }

            if (delayedObs)
            {
               if (!once)
               {
                   for (int i = 0; i < obstacles.Length; i++)
                   {
                       if (i%2 != 0)
                       {
                           obstacles[i].GetComponent<singleObs>().activeSingle = true;
                       }
                   }
                   once = true;
               }
               timing -= Time.deltaTime;
               if (timing <= 0) 
               { 
                   for (int i = 0; i < obstacles.Length; i++)
                   {
                       if (i%2 == 0)
                       {
                           obstacles[i].GetComponent<singleObs>().activeSingle = true;
                       }
                   }
                    timing = delayOnceOpen; 
                    once = false; 
                    delayedObs = false;
               }
            }
        }

    }
    
   
    /* if (!powerOn)
            {
                if (obstacles[countObs].localPosition.x < recoverPos[0].x)
                {
                    obstacles[countObs].localPosition =
                        Vector3.MoveTowards(obstacles[countObs].localPosition, targetPos[countObs], 6 * Time.deltaTime);
                }
                else
                {
                    if (countObs!=obstacles.Length - 1)
                    {
                        countObs++;
                    }
                    else
                    {
                        activateObs = false;
                        countObs = 0;
                    }
                }
            }
  /  private IEnumerator backToNormal()
    {
        countObs = 0;

        while (true)
        {
            if (obstacles[countObs].localPosition.x < recoverPos[0].x)
            {
                obstacles[countObs].localPosition =
                    Vector3.MoveTowards(obstacles[countObs].localPosition, recoverPos[countObs], 6 * Time.deltaTime);
            }
            else
            {
                if (countObs!=obstacles.Length - 1)
                {
                    countObs++;
                }
                else
                {
                    activateObs = false;
                    countObs = 0;
                }
            }

            yield return _waitForFixedUpdate;
        }
    }*/
 
}


