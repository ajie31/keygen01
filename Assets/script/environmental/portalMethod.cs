﻿using System;
using System.Collections;
using Cinemachine;
using UnityEngine;
using UnityEngine.UIElements;

public class portalMethod : MonoBehaviour
{
    public bool insideTeleport;
    public Transform player;
    
    public bool portalDestination = false;
    public bool doingTeleport;

    [SerializeField]private FloatingJoystick _joystick;
    [SerializeField] private cameraManager _cameraManager;

    private RaycastHit2D touched;
    private Touch touch;
    private Vector3 posTeleport = Vector3.zero;
    private float distanceToScreen;


    
    
    void Start()
    {
        distanceToScreen = Camera.main.WorldToScreenPoint(gameObject.transform.position).z;
    }
    
    void FixedUpdate()
    {
  
        if (Input.touchCount > 0 && !portalDestination && insideTeleport && doingTeleport)
        {
            //Debug.Log("here");
            touch = Input.GetTouch(0);
            posTeleport = Camera.main.ScreenToWorldPoint(new Vector3(touch.position.x,
                touch.position.y, distanceToScreen));
            touched = Physics2D.Raycast(posTeleport, Vector2.zero);
           
            if (touched.collider)
            {
              
                if (touched.collider.gameObject.CompareTag("portal"))
                {
                   // Debug.Log("here");
                    portalDestination = true;
                    player.gameObject.SetActive(true);
                    player.position = touched.collider.transform.position;
                    touched.collider.GetComponent<portal>().outRemain = false;
                    touched.collider.GetComponent<portal>().activeDurability -= 1 ;
                    doingTeleport = false;
                    StartCoroutine(_cameraManager.cameraMoveManager(false));
                    audioManager._Audio.playSFX(5);
                    _joystick.gameObject.SetActive(true);

                }
            }
        }
    }


   
}
