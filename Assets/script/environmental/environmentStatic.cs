﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class environmentStatic : MonoBehaviour
{
    // Start is called before the first frame update
    public static environmentStatic current;
    [SerializeField] private Color activeColor;
    [SerializeField] private Color deactiveColor;
    [SerializeField] private Color deactiveColorSwitch;
    [SerializeField] private Color wireAndDoor;

    private void Awake()
    {
        current = this;
    }

    public Color getColor(bool active)
    {

        if (active)
        {
            return activeColor;
        }
        else
        {
            return deactiveColor;
        }
    }
    public Color getColorWD(bool active)
    {

        if (active)
        {
            return wireAndDoor;
        }
        else
        {
            return deactiveColor;
        }
    }
    public Color getColorSwitch(bool active)
    {

        if (active)
        {
            return Color.white;
        }
        else
        {
            return deactiveColorSwitch;
        }
    }
}
