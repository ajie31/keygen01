﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class triggerMethod : MonoBehaviour
{
    public doorTrigger[] doors;
    public portal[] portals;
    public obsMove[] obsMoves;
    public switchCable[] _switches;
    
    //door trigger
    public bool insideDoorTrigger;
    public bool doorOpen;
    
    //end of door trigger
    
    //activation
    public bool insideActivasion;
    public bool allActivate;
    //end of activation
    
    //move obstacle
    public bool insideObsTrigger;

    public bool obsMove;
    //end move obstacle
    
    //portalChannel only

    public bool channelPortal;
    public bool portalReady;

   // private portalMethod _portal;
    //end of portal
    
    //switch trigger
    public bool insideSwitchTrigger;
    public bool switchTrigger;
    //end of switch trigger
    [SerializeField] private cameraManager cameraManager;
    [SerializeField] private Image radialProgress;
    [SerializeField] private Image radialProgressAct;

    private float radialAmount = 0;
    public bool reverse;
    private bool once;
    private bool channellingDoor;
    private float chanelProgress;


    private void Awake()
    {
        Application.targetFrameRate = 30;
    }
    

    private void FixedUpdate()
    {
        // activating all door and activating
        if ((insideDoorTrigger || insideActivasion || insideObsTrigger ||channelPortal||insideSwitchTrigger) && radialAmount < 1  )
        {
           
            if (insideActivasion && !reverse&& !allActivate)
            {
                if (!once)
                {
                    radialProgressAct.gameObject.SetActive(true);
                    once = true;
                }
                radialProgressAct.fillAmount += Time.deltaTime;
            }

            if (insideDoorTrigger || insideObsTrigger || channelPortal||insideSwitchTrigger)
            {
                if (!once)
                {
                    radialProgress.gameObject.SetActive(true);
                    once = true;
                }

                radialAmount += Time.deltaTime;
                radialProgress.fillAmount = radialAmount;
            }
            
    

        }
        //reverse process or deactivating
        if (radialProgressAct.fillAmount > 0 && insideActivasion && reverse && allActivate)
        {
            if (insideActivasion)
            {
                if (!once)
                {
                    radialProgressAct.gameObject.SetActive(true);
                    once = true;
                }
                radialProgressAct.fillAmount -= Time.deltaTime;
            }
        }
       
        //when all activating process and open door is cancelled
        if ((!insideDoorTrigger && !insideActivasion && !insideObsTrigger && !channelPortal &&!insideSwitchTrigger)  && once)
        {
            if (radialProgressAct.fillAmount < 1 && !allActivate && !reverse)
            {
                radialProgressAct.fillAmount = 0;
                radialProgressAct.gameObject.SetActive(false);
            }
            // when canceled load activation on reverse 
            if (radialProgressAct.fillAmount >0 && reverse && allActivate)
            {
           
                radialProgressAct.fillAmount = 1;
                radialProgressAct.gameObject.SetActive(false);
                
            }
            //if progress door done and open
            if (radialAmount>=1 && (doorOpen||obsMove||portalReady||switchTrigger))
            {
                if (doorOpen)
                {
                    doorOpen = false;
                }
                else if(portalReady)
                {
                    portalReady = false;
                }
                else if (switchTrigger)
                {
                    switchTrigger = false;
                }
                else
                {
                    obsMove = false;
                }
                radialAmount = 0;
                
            }
            
            //cancel door trigger
            if (radialAmount < 1)
            {
                radialAmount = 0;
                radialProgress.fillAmount = radialAmount;
                radialProgress.gameObject.SetActive(false);
               // once = false;
            }
            //when load activation done and need to reverse after leave insideActivasion
            if ( radialProgressAct.fillAmount >=1 && !reverse && allActivate)
            {

                reverse = true;
                radialProgressAct.gameObject.SetActive(false);
                //once = false;
            
            }
            //when load activation done and need to unreverse after leave insideActivasion
            if ( radialProgressAct.fillAmount <=0 && reverse && !allActivate)
            {

                reverse = false;
                radialProgressAct.gameObject.SetActive(false);
                //once = false;
            
            }
            once = false;
            
        }

        //if progress door done and open
        if ((insideDoorTrigger||insideObsTrigger || channelPortal||insideSwitchTrigger)&&radialAmount>=1 &&
            (!doorOpen||!obsMove||!portalReady||!switchTrigger))
        {
            if (insideDoorTrigger)
            {
                doorOpen = true;
            }
            else if (channelPortal)
            {
                portalReady = true;
                StartCoroutine(cameraManager.cameraMoveManager(true));
            }
            else if (insideSwitchTrigger)
            {
                switchTrigger = true;
            }
            else
            {
                obsMove = true;
            }
            
        }

        //activate all function
        if (radialProgressAct.fillAmount >=1 && !allActivate )
        {
            StartCoroutine(activateAll());
            allActivate = true;
            //reverse = true;
        }
        //deactivate all function
        if (radialProgressAct.fillAmount <=0 && allActivate )
        {
            StartCoroutine(deActivateAll());
            allActivate = false;
            //reverse = true;
        }
    }

    private IEnumerator activateAll()
    {
        foreach (var portal in portals)
        {
            portal.activate = true;
            yield return null;
        }

        foreach (var door in doors)
        {
            door.activate = true;
            yield return null;
        }
        foreach (var obs in obsMoves)
        {
            obs.activateTriggerObs = true;
            //obs.powerOn = true;
            yield return null;
        }
        foreach (var _switch in _switches)
        {
            _switch.powerSwitch = true;
            //obs.powerOn = true;
            yield return null;
        }
        
    }
    
    private IEnumerator deActivateAll()
    {
        foreach (var portal in portals)
        {
            portal.activate = false;
            yield return null;
        }

        foreach (var door in doors)
        {
            door.activate = false;
            yield return null;
        }
        foreach (var obs in obsMoves)
        {
            obs.activateTriggerObs = false;
            //obs.powerOn = false;
            yield return null;
        }
        foreach (var _switch in _switches)
        {
            _switch.powerSwitch = false;
           
            yield return null;
        }
        
    }
    
    
}
