﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class portal : MonoBehaviour
{
    public bool activate;
    public int activeDurability;
    public bool outRemain = true;
    public bool enterRemain = true;
    private CircleCollider2D _circle;
    private bool currentStat;
    private SpriteRenderer _renderer;
    private Vector3 eulerAngle;
   
    private Quaternion deltaRotation;
    private void Start()
    {
        eulerAngle = new Vector3(0,0,10);
        currentStat = activate;
        _circle = GetComponent<CircleCollider2D>();
        _renderer = GetComponent<SpriteRenderer>();
        //start State
        currentStat = activate;
        _circle.enabled = false;
        _renderer.color = environmentStatic.current.getColor(activate);
    }

    private void Update()
    {

        if (((currentStat != activate) )&& (outRemain || enterRemain))
        {
            //Debug.Log("here");
          
            if (!activate)
            {
                currentStat = activate;
                _circle.enabled = false;
                _renderer.color = environmentStatic.current.getColor(activate);
            }
            else
            {
                currentStat = activate;
                _circle.enabled = true;
                _renderer.color = environmentStatic.current.getColor(activate);
            }
            
        }
        if (activate && ((!outRemain && !enterRemain )|| activeDurability ==0))
        {

            activate = false;
            _renderer.color = environmentStatic.current.getColor(activate);
            _circle.enabled = false;
        }

        if (activate && activeDurability == 1)
        {
            deltaRotation = Quaternion.Euler(eulerAngle* (10 * Time.deltaTime));
            transform.rotation = transform.rotation * deltaRotation;
        }
        if (activate && activeDurability == 2)
        {
            deltaRotation = Quaternion.Euler(eulerAngle* (30 * Time.deltaTime));
            transform.rotation = transform.rotation * deltaRotation;
        }
        
    }
}
