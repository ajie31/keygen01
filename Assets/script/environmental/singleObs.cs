﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class singleObs : MonoBehaviour
{
    
    public bool activeSingle;
    public bool leftRightOnce;
    public bool rotationRight;
    public bool rotationLeft;
    public bool leftRightDelayed;
    public bool closeObs;

    public bool rotateFollow;
    public Transform followRotationObj;
    private AudioSource _AudioManagerObs;
    
    //public bool starting;
    //left and right once property
    private bool reverse;
    private Vector3 target;
    private Vector3 recovery;
    //enf of left and right once
    
    
    //rotation property
    private Vector3 eulerAngle;
    private Quaternion deltaRotation;
    //end of rotation
    
    //delayed LeftRight
    public float setDelay;
    private float delay = 0;

   
    // end of delayed left right

    //from switch
    public bool fromSwitch;
    //end from switch
    
    //close obstacle
    public float targetClose;

    private Vector3 closePost;
    //close obstacle
    private float speed;

    private bool _once;
    private void Awake()
    {
        eulerAngle = new Vector3(0,0,10);
        speed = 5;
    }

    private void Start()
    {
        _AudioManagerObs = GetComponent<AudioSource>();
        if (fromSwitch)
        {
            target = new Vector3(0,transform.localPosition.y,0) + transform.parent.parent.GetComponent<switchTrigger>().targetObs;
        }
        if (!rotationLeft&&!rotationRight && !fromSwitch)
        {
            target = new Vector3(0,transform.localPosition.y,0) + transform.parent.parent.GetComponent<obsMove>().target;
        }
       
        

        if (closeObs)
        {
            closePost = new Vector3(targetClose,transform.localPosition.y);
        }
        
        recovery = transform.localPosition;
       
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (activeSingle)
        {
            if (rotationLeft && !closeObs)
            {
                if (!_once)
                {
                    if (rotateFollow)
                    {
                        transform.rotation = followRotationObj.rotation;
                    }
                    _AudioManagerObs.Play();
                    _once = true;
                }
                deltaRotation = Quaternion.Euler(eulerAngle* (-speed * Time.deltaTime));
                transform.rotation = transform.rotation * deltaRotation;
            }
            if (rotationRight && !closeObs)
            {
                if (!_once)
                {
                    if (rotateFollow)
                    {
                        transform.rotation = followRotationObj.rotation;
                    }
                    _AudioManagerObs.Play();
                    _once = true;
                }
                deltaRotation = Quaternion.Euler(eulerAngle* (speed * Time.deltaTime));
                transform.rotation = transform.rotation * deltaRotation;
            }
            if (leftRightOnce && !closeObs)
            {
                if (!reverse)
                {
                    if (transform.localPosition.x != target.x)
                    {
                        if (!_once)
                        {
                            _AudioManagerObs.Play();
                            _once = true;
                        }
                        transform.localPosition =
                            Vector3.MoveTowards(transform.localPosition, target, 4 * Time.deltaTime);
                    }
                    else
                    {
                        reverse = true;
                    }
                }
            
                else
                {
                    if (transform.localPosition.x != recovery.x)
                    {
                        if (_once)
                        {
                            _AudioManagerObs.Play();
                            _once = false;
                        }
                        transform.localPosition = 
                            Vector3.MoveTowards(transform.localPosition, recovery, 4 * Time.deltaTime);
                    }
                    else
                    {
                        reverse = false;
                        activeSingle = false;

                    }
                }
            }

            if (leftRightDelayed && !closeObs)
            {
                delay -= Time.deltaTime;
                if (delay <= 0 && !reverse)
                {
                    if (!_once)
                    {
                        _AudioManagerObs.Play();
                        _once = true;
                    }
                     if (transform.localPosition.x != target.x)
                     {
                         transform.localPosition =
                             Vector3.MoveTowards(transform.localPosition, target, 6 * Time.deltaTime);
                     }
                     else
                     {
                         if (fromSwitch)
                         {
                             delay = setDelay-.5f; 
                         }
                         else
                         {
                             delay = setDelay-.6f;
                         }
                         //Debug.Log("here");
                         
                         reverse = true;
                         
                     }
                }

                if (delay <= 0 && reverse)
                {
                     if (transform.localPosition.x != recovery.x)
                     {
                         if (_once)
                         {
                             _AudioManagerObs.Play();
                             _once = false;
                         }
                         transform.localPosition =
                             Vector3.MoveTowards(transform.localPosition, recovery, 6f * Time.deltaTime);
                     }
                     else
                     {
                         delay = setDelay ;
                         reverse = false;
                     }
                }
            }

            if (closeObs)
            {
                
                if (transform.localPosition.x > targetClose)
                {
                    if (!_once)
                    {
                        _AudioManagerObs.Play();
                        _once = true;
                    }
                     transform.localPosition =
                         Vector3.MoveTowards(transform.localPosition, closePost, 10 * Time.deltaTime);
                }
                else
                {
                    _once = true;
                    activeSingle = false;
                    closeObs = false;
                    recovery = transform.localPosition;
                   
                }
            }

           
        }
        if (_once && !activeSingle)
        {
            _AudioManagerObs.Stop();
            _once = false;
            
        }

    }
}
