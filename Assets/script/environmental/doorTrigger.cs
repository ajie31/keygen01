﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class doorTrigger : MonoBehaviour
{
    public bool activate;
    public bool doorOpen;
    public bool doorAtStart;
    public Animator animationTriggered;
    private SpriteRenderer _rendererDoor;
    private SpriteRenderer _rendererWire;
    private bool currentDoor;
    private bool currentStat;
    private PolygonCollider2D _collider2D;
    private SpriteRenderer _renderer;
    private string animatorId;
    private void Start()
    {
        _rendererDoor = transform.GetChild(0).GetComponent<SpriteRenderer>();
        _rendererWire = transform.GetChild(2).GetComponent<SpriteRenderer>();
        currentStat = activate;
        currentDoor = doorOpen;
        _collider2D = GetComponent<PolygonCollider2D>();
        _renderer = GetComponent<SpriteRenderer>();
        animatorId = "playTrigger";
        //start state
        currentStat = activate;
        if (doorAtStart)
        {
            _rendererDoor.gameObject.SetActive(false);
        }
        _collider2D.enabled = false;
        _renderer.color = environmentStatic.current.getColor(activate);
        _rendererDoor.color = environmentStatic.current.getColorWD(activate);
        _rendererWire.color = environmentStatic.current.getColorWD(activate);
    }

    private void Update()
    {
        if ((currentStat != activate))
        {
            if (!activate)
            {
                animationTriggered.Play("icon",-1,0f);
                animationTriggered.SetFloat(animatorId,1);
                currentStat = activate;
                if (doorAtStart)
                {
                    _rendererDoor.gameObject.SetActive(false);
                }
                _collider2D.enabled = false;
                _renderer.color = environmentStatic.current.getColor(activate);
                _rendererDoor.color = environmentStatic.current.getColorWD(activate);
                _rendererWire.color = environmentStatic.current.getColorWD(activate);
            }
            else
            {
                
                currentStat = activate;
                if (doorAtStart)
                {
                    _rendererDoor.gameObject.SetActive(true);
                }
                _collider2D.enabled = true;
                _renderer.color = environmentStatic.current.getColor(activate);
                _rendererDoor.color = environmentStatic.current.getColorWD(activate);
                _rendererWire.color = environmentStatic.current.getColorWD(activate);
            }
        }

        if (currentDoor != doorOpen)
        {
            
            currentDoor = doorOpen;
            transform.GetChild(0).gameObject.SetActive(false);
            _rendererWire.color = environmentStatic.current.getColorWD(false);
        }
    }
}
