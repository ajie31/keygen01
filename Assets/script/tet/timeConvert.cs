﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class timeConvert : MonoBehaviour
{
    private float time;
    private int second;
    private int minute;
    private int hours;

    public TextMeshProUGUI gamePlayTimeText;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        time += Time.deltaTime;
    }

    private void secToMin()
    {
        second = (int)time;
        if (second >= 60f)
        {

            minute = (second / 60);
            second = second%60;

            if (minute >= 60)
            {

                hours = (minute / 60);
                minute = minute % 60;

            }

        }


    }

    private void minToHour()
    {
        if (minute >= 60)
        {

            hours =(minute / 60);
            minute = minute % 60;

        }

    }

    public void showTime()
    {

        secToMin();
        gamePlayTimeText.text = hours.ToString("00") + ":" + minute.ToString("00") + ":" + second.ToString("00");
        
    }
}
