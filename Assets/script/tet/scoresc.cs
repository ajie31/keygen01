﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class scoresc : MonoBehaviour
{
    public Slider progressSlider;
    private WaitForSeconds _waitScenods;
    private WaitForFixedUpdate _fixedUpdate;
    private float progressVal;
    private float timing; 
    private int target;
    // Start is called before the first frame update

    private void Start()
    {
        target = 120;
        _waitScenods = new WaitForSeconds(1);
        _fixedUpdate = new WaitForFixedUpdate();
        StartCoroutine(startProgress(150f));
    }
    // Update is called once per frame
    private IEnumerator startProgress(float valB)
    {
        timing = 0;
        progressVal = 0;
        float slideVal = target / valB;
        yield return _waitScenods;
        while (true)
        {
            progressVal = Mathf.Lerp(progressVal,slideVal,timing);
            progressSlider.value = progressVal;
            timing += Time.deltaTime / 2; 
            if(progressVal >= slideVal)
            {
                break;
            }

            yield return _fixedUpdate;
        }

    }
}
