﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;

public class adsManager : MonoBehaviour
{
    private int interStitialCount = 0;
	public static adsManager _Ads;
	[HideInInspector] public double amountReward; 
	private InterstitialAd interstitial;
	private BannerView bannerView;
	private RewardBasedVideoAd rewardBassedVideo;
    // Use this for initialization
    string adUnitIdBann0 = "ca-app-pub-9748959672594642/6762353318";  // "ca-app-pub-3940256099942544/6300978111";
    private string appId = "ca-app-pub-9748959672594642~4375190673";
    string adUnitIdInt = "ca-app-pub-9748959672594642/3212553752";// "ca-app-pub-3940256099942544/1033173712";


    private void Awake()
    {
        if (_Ads == null)
        {
            DontDestroyOnLoad(this);
            _Ads = this;
        }
        else
        {
            if (_Ads != this)
            {
                Destroy(this);
            }
        }
    }

    private  void Start () {

	    if (Application.platform == RuntimePlatform.Android)
	    {
		   

		    MobileAds.Initialize(appId);
		    //this.rewardBassedVideo = RewardBasedVideoAd.Instance;

		    //rewardBassedVideo.OnAdRewarded += HandleRewardBasedVideoRewarded;

		    //RequestRewardVideo();
		    
		   // RequestBanner();
	    }

    }

	private AdRequest CreateAdRequest()
	{
		/*return new AdRequest.Builder()
			.AddTestDevice(AdRequest.TestDeviceSimulator)
			.AddTestDevice("0123456789ABCDEF0123456789ABCDEF")
			.Build();*/

        return new AdRequest.Builder().Build();
    }
	

	public void RequestInterstitial()
	{
		/*#if PLATFORM_ANDROID
			string adUnitId = "ca-app-pub-3940256099942544/1033173712";
			
		//string adUnitId = "ca-app-pub-9748959672594642/3212553752";
			
		#else
			string adUnitId = "unexpected_platform";
		#endif*/
		if(Application.platform == RuntimePlatform.Android)
		{
			
			interstitial = new InterstitialAd(adUnitIdInt);
			interstitial.LoadAd(this.CreateAdRequest());
		}
		
		
		//AdRequest request = new AdRequest.Builder().AddTestDevice("2077ef9a63d2b398840261c8221a0c9b").Build();
		
		//.AddTestDevice("2077ef9a63d2b398840261c8221a0c9b")
	}

	public void RequestRewardVideo()
	{
		/*#if UNITY_ANDROID
			string adUnitId = "ca-app-pub-3940256099942544/5224354917";
		//string adUnitId = "ca-app-pub-9748959672594642/7696697477";
		#else
			string adUnitId = "unexpected_platform";
		#endif*/
		if(Application.platform == RuntimePlatform.Android)
		{
			
			//this.rewardBassedVideo.LoadAd(this.CreateAdRequest(),adUnitId);
		}
		//AdRequest request = new AdRequest.Builder().AddTestDevice("2077ef9a63d2b398840261c8221a0c9b").Build();
		
		
	}
	public void RequestBanner()
	{
	
		if (Application.platform == RuntimePlatform.Android)
		{
			bannerView = new BannerView(adUnitIdBann0, AdSize.Banner, AdPosition.Top);
			bannerView.LoadAd(this.CreateAdRequest());
		}

		// Create a 320x50 banner at the top of the screen.
		
	}

	#region RewardBasedVideo callback handlers
	public void HandleRewardBasedVideoRewarded(object sender, Reward args)
	{
		amountReward = args.Amount;	
	}
	
	#endregion
	public void showInterstitial()
	{
        if (interstitial.IsLoaded() && interStitialCount == 2)
        {
            interstitial.Show();
            interStitialCount = 0;
        }
        else
        {
            interStitialCount += 1;

        }
		
	}

	public void showBanner0()
	{
		this.bannerView.Show();
		
	}

    public void hideDestroyBanner(bool destroy)
    {

        if (destroy)
        {
            this.bannerView.Destroy();
        }
        else
        {
            this.bannerView.Hide();
        }
        
    }
    public void clearInterstitial() {

        if (interstitial.IsLoaded())
        {
            this.interstitial.Destroy();
            
        }
       
    }

	public void delayAds()
	{
		Invoke("showInterstitial", .7f);
	}

	public void watchRewardAd()
	{
		if (rewardBassedVideo.IsLoaded())
		{
			rewardBassedVideo.Show();
		}
		
	}

}
